package template

import (
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

func compile(s string, offset int) (t *Template, err error) {
	t = new(Template)
	if s == "" {
		return t, nil
	}

	begin := 0
	end := 0
	rest := 0
	for {
		if rest > 0 {
			offset += rest
			s = s[rest:]
		}

		if s == "" {
			return t, nil
		}
		r, _ := utf8.DecodeRuneInString(s)
		switch r {
		case lr:
			begin, end, rest, err = SplitT(s, offset)
			tmpl, err := compile(s[begin+1:end], offset+begin)
			if err != nil {
				return nil, err
			}
			tmpl.SetStart(begin + offset)
			t.append("", "", tmpl)
		case transfo:
			begin, end, rest, err = SplitM(s, offset)
			if err != nil {
				return nil, err
			}
			modifiers, err := makeMods(s[begin:end], offset+begin, []modifier{})
			if err != nil {
				return nil, err
			}
			t.modifiers = modifiers
			ilast := len(t.tripels) - 1
			if ilast >= 0 {
				last := t.tripels[ilast]
				text := last.text
				if text != "" && len(t.modifiers) != 0 && last.placeholder == "" && last.t == nil {
					wspace := ""
					for len(text) > 0 {
						r, size := utf8.DecodeLastRuneInString(text)
						if !unicode.IsSpace(r) {
							break
						}
						wspace = string(r) + wspace
						text = text[:len(text)-size]
					}
					if wspace != "" {
						last.text = text
						t.tripels[ilast] = last
						t.wspace = wspace
					}
				}
			}
		default:
			begin, end, rest, err = SplitP(s, offset)
			if err != nil {
				return nil, err
			}
			text := ""
			if end == 0 {
				text = unEscape(s)
			} else {
				text = unEscape(s[:begin])
			}
			placeholder := ""
			if end != begin {
				placeholder = s[begin : end+1]
			}
			t.append(text, placeholder, nil)
		}
	}
}

func unEscape(s string) string {
	var buf strings.Builder
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		if r == escape && len(s) != size {
			s = s[size:]
			r, size = utf8.DecodeRuneInString(s)
		}
		s = s[size:]
		buf.WriteRune(r)
	}
	return buf.String()
}

func setEscape(s string) string {
	var buf strings.Builder
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		s = s[size:]
		if r == escape || r == lr || r == rr || r == transfo || r == dollar {
			buf.WriteRune(escape)
		}
		buf.WriteRune(r)
	}
	return buf.String()
}

// SplitP splits a string to the first occurence of a placeholder
//
//	pbegin: index in s of the `$` of the first placeholder
//	pend: index in s of the following rune of the placeholder
//	prest: index of the position where next calculataion can occur
func SplitP(s string, offset int) (pbegin int, pend int, prest int, err error) {
	length := len(s)
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		switch r {
		case escape:
			if len(s) == size {
				err = fmt.Errorf("ESCAPE-UNFINISHED: escape sequence started but not finished at position %d", offset)
				return
			}
			s = s[size:]
			offset += size
			r, size = utf8.DecodeRuneInString(s)
			if r != transfo && r != dollar && r != escape && r != lr && r != rr {
				err = fmt.Errorf("ESCAPE-BADRUNE: character `%c` should not be escaped at position %d", r, offset)
				return
			}
		case lr, transfo:
			n := length - len(s)
			return n, n, n, nil

		case rr:
			err = fmt.Errorf("UNESCAPED-RR: escape sequence started but not finished at position %d", offset)
			return
		case dollar:
			if s == "" {
				err = fmt.Errorf("ESCAPE-UNFINISHED: escape sequence started but not finished at position %d", offset)
				return
			}
			pbegin = length - len(s)
			pend = pbegin
			s = s[size:]
			offset += size
			for len(s) > 0 {
				r, size := utf8.DecodeRuneInString(s)
				if !unicode.IsLetter(r) {
					if pbegin == pend {
						err = fmt.Errorf("PLACEHOLDER-NOTALETTER: placeholder should start with a letter at position %d", offset)
						break
					}
					if !unicode.IsNumber(r) {
						break
					}
				}
				s = s[size:]
				offset += size
				pend += size
			}
			if pbegin == pend {
				err = fmt.Errorf("PLACEHOLDER-EMPTY: placeholder should not be empty at position %d", offset)
			}
			if err != nil {
				return 0, 0, 0, err
			}
			return pbegin, pend, pend + 1, nil
		}
		s = s[size:]
		offset += size
	}
	return 0, 0, length - len(s), nil
}

// SplitT splits a string to the first occurence of a template
//
//	tbegin: index in s of the `{` of the first template
//	tend: index in s of the corresponding `}` of the template
//	trest: index of the position where next calculation can occur
func SplitT(s string, offset int) (tbegin int, tend int, trest int, err error) {
	length := len(s)
	inquote1 := false
	inquote2 := false
	level := 0
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		switch r {
		case escape:
			if !inquote2 {
				if len(s) == size {
					err = fmt.Errorf("ESCAPE-UNFINISHED: escape sequence started but not finished at position %d", offset)
					return
				}
				s = s[size:]
				offset += size
				_, size = utf8.DecodeRuneInString(s)
			}
		case lr:
			if !inquote1 && !inquote2 {
				if level == 0 {
					tbegin = length - len(s)
				}
				level++
			}
		case rr:
			if !inquote1 && !inquote2 {
				level--
				if level < 0 {
					err = fmt.Errorf("TOO-MANY-RR: too many `%c` detected at position %d", rr, offset)
					return 0, 0, 0, err
				}
				if level == 0 {
					tend = length - len(s)
					return tbegin, tend, tend + size, nil
				}
			}
		case quote1:
			if !inquote2 {
				inquote1 = !inquote1
			}
		case quote2:
			if !inquote1 {
				inquote2 = !inquote2
			}
		}
		s = s[size:]
		offset += size
	}

	if level != 0 {
		return 0, 0, 0, fmt.Errorf("TOO-MANY-LR: too many `%c` detected", lr)
	}
	return 0, 0, length, nil
}

// SplitTrans splits a string to the first occurence of a modifier
//    tbegin: index in s of the `[]` of the first modifier
//    tend: index in s of the corresponding `}` of the template
//    trest: index of the position where next calculation can occur

func SplitM(s string, offset int) (tbegin int, tend int, trest int, err error) {
	length := len(s)
	inquote1 := false
	inquote2 := false
	intransfo := false
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		switch r {
		case escape:
			if !inquote2 {
				if len(s) == size {
					err = fmt.Errorf("ESCAPE-UNFINISHED: escape sequence started but not finished at position %d", offset)
					return
				}
				s = s[size:]
				offset += size
				_, size = utf8.DecodeRuneInString(s)
			}
		case transfo:
			if !intransfo {
				intransfo = true
				tbegin = length - len(s)
			}
		case rr:
			if intransfo && !inquote1 && !inquote2 {
				tend = length - len(s)
				return tbegin, tend, tend, nil
			}
		case quote1:
			if !inquote2 {
				inquote1 = !inquote1
			}
		case quote2:
			if !inquote1 {
				inquote2 = !inquote2
			}
		}
		s = s[size:]
		offset += size
	}
	if !intransfo {
		return 0, 0, length, nil
	}
	return tbegin, length, length, nil
}

func makeMods(s string, offset int, modifiers []modifier) ([]modifier, error) {
	var err error
	var buf strings.Builder
	for len(s) > 0 {
		r, size := utf8.DecodeRuneInString(s)
		s = s[size:]
		offset += size
		switch r {
		case quote2:
			ok := false
			buf.WriteRune(r)
			for len(s) > 0 {
				r, size := utf8.DecodeRuneInString(s)
				s = s[size:]
				offset += size
				buf.WriteRune(r)
				if r == quote2 {
					ok = true
					break
				}
			}
			if !ok {
				return modifiers, fmt.Errorf("QUOTE2-MISSING: could not find end \"%c\"", quote2)
			}
		case quote1:
			ok := false
			buf.WriteRune(r)
			esc := false
			for len(s) > 0 {
				r, size := utf8.DecodeRuneInString(s)
				s = s[size:]
				offset += size
				buf.WriteRune(r)
				if r == escape {
					esc = !esc
					continue
				}
				if !esc && r == quote1 {
					ok = true
					break
				}
			}
			if !ok {
				return modifiers, fmt.Errorf("QUOTE1-MISSING: could not find end `%c`", quote1)
			}
		case transfo:
			if len(s) > 0 {
				r, size := utf8.DecodeRuneInString(s)
				if r == transfo {
					buf.WriteRune(r)
					buf.WriteRune(r)
					s = s[size:]
					offset += size
					continue
				}
			}
			x := strings.TrimSpace(buf.String())
			buf.Reset()
			if x != "" {
				modifiers, err = addpexpr(modifiers, x)
				if err != nil {
					return modifiers, err
				}
			}
			modifiers, err = addpexpr(modifiers, "|")
			if err != nil {
				return modifiers, err
			}
		default:
			if unicode.IsSpace(r) {
				x := strings.TrimSpace(buf.String())
				buf.Reset()
				if x != "" {
					modifiers, err = addpexpr(modifiers, x)
					if err != nil {
						return modifiers, err
					}
				}
			} else {
				buf.WriteRune(r)
			}
		}
	}
	x := strings.TrimSpace(buf.String())
	if x != "" {
		modifiers, err = addpexpr(modifiers, x)
	}
	return modifiers, err
}
