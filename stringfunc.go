package template

import (
	"fmt"
	"html"
	"math"
	"net/url"
	"regexp"
	"strings"
	"unicode/utf8"

	"template/pkg/expr"
	"template/pkg/rstrings"
)

func SwapCase(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("swapcase works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.SwapCase(str), nil
}

func Upper(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("upper works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return strings.ToUpper(str), nil
}

func Upper1(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("upper1 works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.FirstRuneToUpper(str), nil
}

func Lower(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("lower works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return strings.ToLower(str), nil
}

func Lower1(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("lower1 works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.FirstRuneToLower(str), nil
}

func Suffix(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("suffix works with 1 argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	suffix, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert suffix to string")
	}
	return str + suffix, nil
}

func Prefix(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("prefix works with 1 argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	prefix, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert suffix to string")
	}
	return prefix + str, nil
}

func Repeat(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("repeat works with 1 argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	repeat, err := expr.ToInt64(args[0])
	if err != nil || repeat < 0 || repeat > math.MaxInt {
		return nil, fmt.Errorf("cannot convert repeater to positive integer")
	}

	return strings.Repeat(str, int(repeat)), nil
}

func TrimSpace(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("trimspace works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return strings.TrimSpace(str), nil
}

func TrimSpaceLeft(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("trimspaceleft works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.TrimSpaceLeft(str), nil
}

func TrimSpaceRight(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("trimspaceright works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.TrimSpaceRight(str), nil
}

func Trim(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("trim works with 1 argument: the cutset")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	cutset, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert cutset to string")
	}
	if cutset == "" {
		return strings.Trim(str, cutset), nil
	}
	if !strings.ContainsAny(cutset, "-^") {
		return strings.Trim(str, cutset), nil
	}
	tr := rstrings.NewTranslator(cutset)
	if tr == nil {
		return strings.Trim(str, cutset), nil
	}

	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		if _, matched := tr.TranslateRune(r); !matched {
			break
		}
		str = str[size:]
	}
	for len(str) > 0 {
		r, size := utf8.DecodeLastRuneInString(str)
		if _, matched := tr.TranslateRune(r); !matched {
			break
		}
		str = str[:len(str)-size]
	}
	return str, nil
}

func TrimLeft(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("trimLeft works with 1 argument: the cutset")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	cutset, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert cutset to string")
	}
	if cutset == "" {
		return strings.TrimLeft(str, cutset), nil
	}
	if !strings.ContainsAny(cutset, "-^") {
		return strings.TrimLeft(str, cutset), nil
	}
	tr := rstrings.NewTranslator(cutset)
	if tr == nil {
		return strings.TrimLeft(str, cutset), nil
	}

	for len(str) > 0 {
		r, size := utf8.DecodeRuneInString(str)
		if _, matched := tr.TranslateRune(r); !matched {
			break
		}
		str = str[size:]
	}

	return str, nil
}

func TrimRight(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("trimright works with 1 argument: the cutset")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	cutset, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert cutset to string")
	}
	if cutset == "" {
		return strings.TrimRight(str, cutset), nil
	}
	if !strings.ContainsAny(cutset, "-^") {
		return strings.TrimRight(str, cutset), nil
	}
	tr := rstrings.NewTranslator(cutset)
	if tr == nil {
		return strings.TrimRight(str, cutset), nil
	}

	for len(str) > 0 {
		r, size := utf8.DecodeLastRuneInString(str)
		if _, matched := tr.TranslateRune(r); !matched {
			break
		}
		str = str[:len(str)-size]
	}
	return str, nil
}

func TrimPrefix(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("trimprefix works with 1 argument: the prefix to trim")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	prefix, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert prefix to string")
	}
	return strings.TrimPrefix(str, prefix), nil
}

func TrimSuffix(s any, args ...any) (any, error) {
	if len(args) != 1 {
		return nil, fmt.Errorf("trimright works with 1 argument: the suffix to trim")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	suffix, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert suffix to string")
	}
	return strings.TrimSuffix(str, suffix), nil
}

func Delete(s any, args ...any) (any, error) {
	if len(args) == 0 {
		return "", nil
	}

	if len(args) != 1 {
		return nil, fmt.Errorf("delete works with 1 argument: pattern on the characters to be deleted")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	dels, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert delete pattern to string")
	}
	return rstrings.Delete(str, dels), nil
}

func Reverse(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("reverse works withhout additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	return rstrings.Reverse(str), nil
}

func JustifyLeft(s any, args ...any) (any, error) {
	if len(args) == 0 || len(args) > 2 {
		return nil, fmt.Errorf("justifyLeft works with 1 or 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	length, err := expr.ToInt64(args[0])
	if err != nil || length < 0 || length > math.MaxInt {
		return nil, fmt.Errorf("cannot convert length to positive integer")
	}
	pad := " "
	if len(args) == 2 {
		pad, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert padding to string")
		}
	}

	return rstrings.JustifyLeft(str, int(length), pad), nil
}

func JustifyRight(s any, args ...any) (any, error) {
	if len(args) == 0 || len(args) > 2 {
		return nil, fmt.Errorf("justifyRight works with 1 or 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	length, err := expr.ToInt64(args[0])
	if err != nil || length < 0 || length > math.MaxInt {
		return nil, fmt.Errorf("cannot convert length to positive integer")
	}
	pad := " "
	if len(args) == 2 {
		pad, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert padding to string")
		}
	}

	return rstrings.JustifyRight(str, int(length), pad), nil
}

func Center(s any, args ...any) (any, error) {
	if len(args) == 0 || len(args) > 2 {
		return nil, fmt.Errorf("center works with 1 or 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	length, err := expr.ToInt64(args[0])
	if err != nil || length < 0 || length > math.MaxInt {
		return nil, fmt.Errorf("cannot convert length to positive integer")
	}
	pad := " "
	if len(args) == 2 {
		pad, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert padding to string")
		}
	}

	return rstrings.Center(str, int(length), pad), nil
}

func Replace(s any, args ...any) (any, error) {
	if len(args) == 0 || len(args) > 3 {
		return nil, fmt.Errorf("replace works with 1, 2 or 3 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	search, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert searcher to string")
	}

	replace := ""
	if len(args) > 1 {
		replace, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert replacer to string")
		}
	}

	index := -1

	if len(args) == 3 {
		x, err := expr.ToInt64(args[2])
		if err != nil || x < 0 || x > math.MaxInt {
			return nil, fmt.Errorf("cannot convert index to positive integer")
		}
		index = int(x)
	}
	return strings.Replace(str, search, replace, index), nil
}

func ReplaceRegexp(s any, args ...any) (any, error) {
	if len(args) == 0 || len(args) > 3 {
		return nil, fmt.Errorf("replaceRegexp works with 1, 2 or 3 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	search := args[0]

	var rex *regexp.Regexp
	switch v := search.(type) {
	case *regexp.Regexp:
		rex = v
	case regexp.Regexp:
		rex = &v
	default:
		search, err := expr.ToString(args[0])
		if err != nil {
			return nil, fmt.Errorf("cannot convert searcher to regexp")
		}
		rex, err = regexp.Compile(search)
		if err != nil {
			return nil, fmt.Errorf("cannot convert searcher to regexp")
		}
	}
	replace := ""
	if len(args) > 1 {
		replace, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert replacer to string")
		}
	}

	literal := false

	if len(args) == 3 {
		literal, err = expr.ToBool(args[2])
		if err != nil {
			return nil, fmt.Errorf("cannot convert 'literal' to bool")
		}
	}
	if literal {
		return rex.ReplaceAllLiteralString(str, replace), nil
	} else {
		return rex.ReplaceAllString(str, replace), nil
	}
}

func Partition(s any, args ...any) (any, error) {
	if len(args) != 2 {
		return nil, fmt.Errorf("partition works with 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	sep, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert separator to string")
	}

	if args[1] == nil {
		return nil, fmt.Errorf("cannot convert second argument to template")
	}
	head, match, tail := rstrings.Partition(str, sep)
	k := map[string]any{
		"head":  head,
		"match": match,
		"tail":  tail,
	}

	return MakeTransformation(k, args[1])
}

func PartitionLast(s any, args ...any) (any, error) {
	if len(args) != 2 {
		return nil, fmt.Errorf("partitionlast works with 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	sep, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert separator to string")
	}

	if args[1] == nil {
		return nil, fmt.Errorf("cannot convert second argument to template")
	}
	head, match, tail := rstrings.PartitionLast(str, sep)
	k := map[string]any{
		"head":  head,
		"match": match,
		"tail":  tail,
	}

	return MakeTransformation(k, args[1])
}

func Translate(s any, args ...any) (any, error) {
	if len(args) != 2 {
		return nil, fmt.Errorf("translate works with 2 arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	from, err := expr.ToString(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot convert `from` to string")
	}

	to, err := expr.ToString(args[1])

	if err != nil {
		return nil, fmt.Errorf("cannot convert `from` to string")
	}

	return rstrings.Translate(str, from, to), nil
}

func ToCamelCase(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("toCamelCase works with no additional argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	return rstrings.ToCamelCase(str), nil
}

func ToKebabCase(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("toKebabCase works with no additional argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	return rstrings.ToKebabCase(str), nil
}

func ToSnakeCase(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("toSnakeCase works with no additional argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}

	return rstrings.ToSnakeCase(str), nil
}

func Squeeze(s any, args ...any) (any, error) {
	if len(args) != 1 && len(args) != 0 {
		return nil, fmt.Errorf("squeeze works with 1 argument at most: the characters to trim")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	squeeze := ""
	if len(args) == 1 {
		squeeze, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert squeeze to string")
		}
	}

	return rstrings.Squeeze(str, squeeze), nil
}

func SubString(s any, args ...any) (any, error) {
	if len(args) != 1 && len(args) != 2 {
		return nil, fmt.Errorf("slice works with 1 argument or 2 argument")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	a, err := expr.ToInt64(args[0])
	if err != nil || a < math.MinInt || a > math.MaxInt {
		return nil, fmt.Errorf("cannot convert begin to integer")
	}
	if len(args) == 1 {
		return rstrings.SubString(str, int(a)), nil
	}
	b, err := expr.ToInt64(args[1])
	if err != nil || b < math.MinInt || b > math.MaxInt {
		return nil, fmt.Errorf("cannot convert end to integer")
	}
	return rstrings.SubString(str, int(a), int(b)), nil

}

func ASCII(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("encode works withhout additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.ASCII(str), nil
}

func Latin1(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("encode works withhout additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return rstrings.Latin1(str), nil
}

func XML(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("encode works withhout additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return html.EscapeString(str), nil

}

func URL(s any, args ...any) (any, error) {
	if len(args) != 0 {
		return nil, fmt.Errorf("encode works withhout additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return url.QueryEscape(str), nil
}

func Fields(s any, args ...any) (any, error) {
	min := 3
	max := 4
	l := len(args)
	if l < min && l > max {
		return nil, fmt.Errorf("number of arguments for fields is between 3 and 4")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	sep, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert separator to string")
	}

	joiner, err := expr.ToString(args[1])
	if err != nil {
		return nil, fmt.Errorf("cannot convert joiner to string")
	}

	a, err := expr.ToInt64(args[2])
	if err != nil || a < math.MinInt || a > math.MaxInt {
		return nil, fmt.Errorf("cannot convert begin to integer")
	}
	if len(args) == 3 {
		return rstrings.Fields(str, sep, joiner, int(a)), nil
	}
	b, err := expr.ToInt64(args[3])
	if err != nil || b < math.MinInt || b > math.MaxInt {
		return nil, fmt.Errorf("cannot convert end to integer")
	}
	return rstrings.Fields(str, sep, joiner, int(a), int(b)), nil

}
