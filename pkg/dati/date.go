package dati

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"template/pkg/rstrings"
)

func ISO2Date(s string) (t *time.Time, err error) {
	regex := regexp.MustCompile("[^0-9]+")
	ss := regex.ReplaceAllString(s, "-")
	ss = strings.Trim(ss, "-")
	count := strings.Count(ss, "-")
	switch count {
	case 0:
		if len(ss) != 8 {
			return nil, fmt.Errorf("%s is invalid ISO date format", s)
		}
		ss = s[:4] + "-" + s[4:6] + "-" + s[6:]
		return ISO2Date(ss)
	case 2:
		parts := strings.SplitN(ss, "-", -1)
		if len(parts[0]) == 2 {
			now := time.Now()
			year := strconv.Itoa(now.Year())
			ss = year[:2] + ss
			return ISO2Date(ss)
		}
		if len(parts[0]) != 4 {
			return nil, fmt.Errorf("%s is invalid ISO date format", s)
		}
		if len(parts[1]) == 1 {
			ss = ss[:4] + "-0" + parts[1] + "-" + parts[2]
			return ISO2Date(ss)
		}
		if len(parts[1]) != 2 {
			return nil, fmt.Errorf("%s is invalid ISO date format", s)
		}
		if len(parts[1]) == 1 {
			ss = ss[:4] + parts[1] + "-0" + parts[2]
			return ISO2Date(ss)
		}
		if len(parts[2]) != 2 {
			return nil, fmt.Errorf("%s is invalid ISO date format", s)
		}
		tv, err := time.Parse(time.DateOnly, ss)
		if err != nil {
			return nil, fmt.Errorf("%s is invalid ISO date format", s)
		}
		return &tv, nil
	default:
		return nil, fmt.Errorf("%s is invalid ISO date format", s)

	}
}

func Parts(t *time.Time, lang string) (parts map[string]string) {
	if t == nil {
		now := time.Now()
		t = &now
	}
	parts = make(map[string]string)
	y := t.Year()
	s := strconv.Itoa(y)
	parts["year"] = s
	s = "0000" + s
	parts["year2"] = s[len(s)-2:]

	leap := "0"
	switch {
	case y%400 == 0:
		leap = "1"
	case y%4 != 0:
		leap = "0"
	case y%100 == 0:
		leap = "0"
	default:
		leap = "1"
	}
	parts["leap"] = leap

	m := int(t.Month())
	parts["monthno"] = strconv.Itoa(m)
	parts["monthno2"] = fmt.Sprintf("%02d", m)
	parts["month"] = Month(m, lang, -1)
	parts["month2"] = Month(m, lang, 2)
	parts["month3"] = Month(m, lang, 3)

	d := t.Day()
	w := t.Weekday()
	parts["dayno"] = strconv.Itoa(d)
	parts["day2"] = fmt.Sprintf("%02d", d)
	parts["weekday"] = Day(w, lang, -1)
	parts["weekday2"] = Day(w, lang, 2)
	parts["weekday3"] = Day(w, lang, 3)

	h := t.Hour()
	parts["hour"] = strconv.Itoa(h)
	parts["hour2"] = fmt.Sprintf("%02d", h)

	m = t.Minute()
	parts["minute"] = strconv.Itoa(m)
	parts["minute2"] = fmt.Sprintf("%02d", m)

	sec := t.Minute()
	parts["second"] = strconv.Itoa(sec)
	parts["second2"] = fmt.Sprintf("%02d", sec)

	parts["isodate"] = t.Format("2006-01-25")

	return

}
func Format(t *time.Time, format string, lang string) (datetime string, err error) {
	if t == nil {
		now := time.Now()
		t = &now
	}
	if format == "" {
		return t.Format("2006-01-02"), nil
	}

	parts, balanced := rstrings.ParseBrackets(format, '{', '}')
	if len(parts) == 0 {
		return format, nil
	}
	if !balanced {
		return "", fmt.Errorf("'{' and '}' are not balanced")
	}

	start := 0
	for _, part := range parts {
		begin := part[0]
		end := part[1]
		if start < begin {
			datetime += rstrings.UnEscape(format[start:begin], "{}", '\\')
		}
		start = end + 1
		label := format[begin+1 : end]
		switch label {
		case "Y", "Y2":
			s := strconv.Itoa(t.Year())
			if label == "Y2" && len(s) > 1 {
				s = s[len(s)-2:]
			}
			datetime += s
		case "M", "M2":
			m := int(t.Month())
			if label == "M2" {
				datetime += fmt.Sprintf("%02d", m)
			} else {
				datetime += fmt.Sprintf("%d", m)
			}
		case "ML", "M3L", "MU", "M3U":
			m := int(t.Month())
			length := -1
			if strings.Contains(label, "3") {
				length = 3
			}
			maand := Month(m, lang, length)
			if strings.HasPrefix(label, "U") {
				maand = rstrings.FirstRuneToUpper(maand)
			}
			datetime += maand
		case "D", "D2":
			d := t.Day()
			if strings.Contains(label, "2") {
				datetime += fmt.Sprintf("%02d", d)
			} else {
				datetime += fmt.Sprintf("%d", d)
			}
		case "WL", "W3L", "W2L", "WU", "W3U", "W2U":
			d := t.Weekday()
			length := -1
			if strings.Contains(label, "3") {
				length = 3
			}
			if strings.Contains(label, "2") {
				length = 2
			}
			dag := Day(d, lang, length)
			if strings.Contains(label, "U") {
				dag = rstrings.FirstRuneToUpper(dag)
			}
			datetime += dag
		case "h", "h2":
			h := t.Hour()
			if strings.Contains(label, "2") {
				datetime += fmt.Sprintf("%02d", h)
			} else {
				datetime += fmt.Sprintf("%d", h)
			}
		case "m", "m2":
			m := t.Minute()
			if strings.Contains(label, "2") {
				datetime += fmt.Sprintf("%02d", m)
			} else {
				datetime += fmt.Sprintf("%d", m)
			}
		case "s", "s2":
			s := t.Second()
			if strings.Contains(label, "2") {
				datetime += fmt.Sprintf("%02d", s)
			} else {
				datetime += fmt.Sprintf("%d", s)
			}
		default:
			return "", fmt.Errorf("`%s`: unknown indicator for datetime format", label)
		}
	}
	if start < len(format) {
		datetime += rstrings.UnEscape(format[start:], "{}", '\\')
	}
	return datetime, nil

}
