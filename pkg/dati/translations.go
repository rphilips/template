package dati

import (
	"strings"
	"time"
)

var MonthNames = map[string][]string{
	"fre": {"janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"},
	"eng": {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"},
	"dut": {"januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"},
}

var Months31 = map[int]bool{
	1:  true,
	3:  true,
	5:  true,
	7:  true,
	8:  true,
	10: true,
	12: true,
}

var Months30 = map[int]bool{
	4:  true,
	6:  true,
	9:  true,
	11: true,
}

var DayNames = map[string][]string{
	"fre": {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"},
	"eng": {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"},
	"dut": {"maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"},
}

func Month[M time.Month | int | int64](monthno M, lang string, length int) string {
	nr := int(monthno)
	m := MonthNames[lang][nr-1]
	if length > 0 {
		mr := []rune(m)
		if length <= len(mr) {
			return string(mr[:length])
		}
	}
	return m
}

func Day[D time.Weekday | int | int64](dayno D, lang string, length int) string {
	nr := int(dayno)
	if nr == 0 {
		nr = 7
	}
	m := DayNames[lang][nr-1]
	if length > 0 {
		mr := []rune(m)
		if length <= len(mr) {
			return string(mr[:length])
		}
	}
	return m
}

func IndexMonth(start string, lang string) int {
	if len(start) < 3 {
		return -1
	}
	found := -1
	start = strings.ToLower(start)
	for lg := range MonthNames {
		if lang != "" && lg != lang {
			found = -1
			for i, name := range MonthNames[lg] {
				if !strings.HasPrefix(strings.ToLower(name), start) {
					continue
				}
				if found != -1 {
					return -1
				}
				found = i + 1
			}
		}
		if found >= 0 {
			return found
		}
	}
	return -1
}
