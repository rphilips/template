package dati

import (
	"testing"
	"time"
)

func TestFormat(t *testing.T) {
	type args struct {
		t      time.Time
		format string
	}
	tests := []struct {
		name         string
		args         args
		wantDatetime string
		wantErr      bool
	}{
		// TODO: Add test cases.
		{
			name: "t1",
			args: args{
				t:      time.Date(2009, 1, 7, 20, 34, 58, 651387237, time.UTC),
				format: "Iso-time {Y}-{M2}-{D2}",
			},
			wantDatetime: "Iso-time 2009-01-07",
		},
		{
			name: "t2",
			args: args{
				t:      time.Date(2009, 1, 7, 20, 34, 58, 651387237, time.UTC),
				format: "Iso-time {Y}-{M3L}-{D}, {W3U} abc",
			},
			wantDatetime: "Iso-time 2009-jan-7, Woe abc",
		},
		{
			name: "t3",
			args: args{
				t:      time.Date(2009, 1, 7, 20, 34, 58, 651387237, time.UTC),
				format: "Iso-time {Y}-{M2}-{D2}  {h2}:{m2}:{s2}",
			},
			wantDatetime: "Iso-time 2009-01-07  20:34:58",
		},
		{
			name: "t4",
			args: args{
				t:      time.Date(2024, 3, 10, 20, 34, 58, 651387237, time.UTC),
				format: "Iso-time {Y}-{M2}-{D2}  {h2}:{m2}:{s2} {WL}",
			},
			wantDatetime: "Iso-time 2024-03-10  20:34:58 zondag",
		},
		{
			name: "t5",
			args: args{
				t:      time.Date(2024, 11, 29, 20, 34, 58, 651387237, time.UTC),
				format: "Iso-time {D2} {M3L} {Y}",
			},
			wantDatetime: "Iso-time 29 nov 2024",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotDatetime, err := Format(&tt.args.t, tt.args.format, "dut")
			if (err != nil) != tt.wantErr {
				t.Errorf("Format() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotDatetime != tt.wantDatetime {
				t.Errorf("Format() = %v, want %v", gotDatetime, tt.wantDatetime)
			}
		})
	}
}
