package expr

import (
	"fmt"
	"reflect"
	"strings"
	"unicode/utf8"
)

func unarylen(t *Token) (err error) {
	if t.value == nil {
		t.value = 0
		t.nature = nInt
		return
	}
	result := -1
	switch t.nature {
	case nString, nInt:
		s, err := ToString(t.value)
		if err != nil {
			return err
		}
		result = utf8.RuneCountInString(s)
	default:
		rt := reflect.TypeOf(t.value)
		switch rt.Kind() {
		case reflect.Slice, reflect.Array, reflect.Map:
			result = reflect.ValueOf(t.value).Len()
		default:
			t.nature = nString
			return unarylen(t)
		}
	}
	t.value, err = ToInt64(result)
	if err == nil {
		return err
	}
	t.nature = nInt
	return nil
}

func unaryminus(t *Token) (err error) {
	if t.value == nil {
		t.value = 0
		t.nature = nInt
		return
	}
	switch t.nature {
	case nInt:
		i, err := ToInt64(t.value)
		if err != nil {
			return err
		}
		t.value = -i
		t.nature = nInt
	case nBool:
		b, err := ToBool(t.value)
		if err != nil {
			return err
		}
		t.value = !b
		t.nature = nBool
	case nString:
		s, err := ToString(t.value)
		if err != nil {
			return err
		}
		x := strings.TrimLeft(s, "1234567890+-")
		if len(x) == len(s) {
			t.value = int64(0)
			t.nature = nInt
		}
		s = s[:len(x)]
		i, err := ToInt64(s)
		if err != nil {
			return fmt.Errorf("cannot find a number in token")
		}
		t.value = -i
		t.nature = nInt
	case nVar:
		rt := reflect.TypeOf(t.value)
		switch rt.Kind() {
		case reflect.String:
			t.nature = nString
			return unaryminus(t)
		case reflect.Bool:
			t.nature = nBool
			return unaryminus(t)
		default:
			t.nature = nInt
			return unaryminus(t)
		}
	default:
		return fmt.Errorf("cannot find a number in token")
	}
	return nil
}

func unaryplus(t *Token) (err error) {
	switch t.nature {
	case nInt:
		i, err := ToInt64(t.value)
		if err != nil {
			return err
		}
		t.value = i
		t.nature = nInt
	case nBool:
		b, err := ToBool(t.value)
		if err != nil {
			return err
		}
		t.value = b
		t.nature = nBool
	case nString:
		s, err := ToString(t.value)
		if err != nil {
			return err
		}
		x := strings.TrimLeft(s, "1234567890+-")
		if len(x) == len(s) {
			t.value = int64(0)
			t.nature = nInt
		}
		s = s[:len(x)]
		i, err := ToInt64(s)
		if err != nil {
			return fmt.Errorf("cannot find a number in token")
		}
		t.value = i
		t.nature = nInt
	case nVar:
		rt := reflect.TypeOf(t.value)
		switch rt.Kind() {
		case reflect.String:
			t.nature = nString
			return unaryplus(t)
		case reflect.Bool:
			t.nature = nBool
			return unaryplus(t)
		default:
			t.nature = nInt
			return unaryplus(t)
		}
	default:
		return fmt.Errorf("cannot find a number in token")
	}
	return nil
}

func unarynegate(t *Token) (err error) {
	if t.value == nil {
		t.value = true
		t.nature = nBool
		return nil
	}
	switch t.nature {
	case nInt:
		i, err := ToInt64(t.value)
		if err != nil {
			return err
		}
		t.value = i == 0
		t.nature = nBool
	case nBool:
		b, err := ToBool(t.value)
		if err != nil {
			return err
		}
		t.value = !b
		t.nature = nBool
	case nString:
		s, err := ToString(t.value)
		if err != nil {
			return err
		}
		t.value = s == ""
		t.nature = nBool
	case nVar:
		rt := reflect.TypeOf(t.value)
		switch rt.Kind() {
		case reflect.String:
			t.nature = nString
			return unarynegate(t)
		case reflect.Bool:
			t.nature = nBool
			return unarynegate(t)
		default:
			t.nature = nInt
			return unarynegate(t)
		}
	default:
		return fmt.Errorf("cannot find a number in token")
	}
	return nil

}

func unarydoublenegate(t *Token) (err error) {
	err = unarynegate(t)
	if err == nil {
		err = unarynegate(t)
	}
	return err
}
