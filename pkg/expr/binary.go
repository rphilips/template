package expr

import (
	"fmt"
	"math"
	"reflect"
	"regexp"
	"strings"

	"github.com/spf13/cast"
)

func binarymath(opr1, opr2 *Token, op string) (err error) {
	i1, err := cast.ToInt64E(opr1.value)
	if err != nil {
		return
	}
	i2, err := cast.ToInt64E(opr2.value)
	if err != nil {
		return
	}
	switch op {
	case "+":
		opr1.value = i1 + i2
	case "-":
		opr1.value = i1 - i2
	case "*":
		opr1.value = i1 * i2
	case "%":
		opr1.value = i1 % i2
	case "/":
		opr1.value = i1 / i2
	case "^":
		opr1.value = int64(math.Pow(float64(i1), float64(i2)))
	default:
		return fmt.Errorf("math-operator `%s` is not defined", op)
	}
	opr1.nature = nInt
	return
}

func binarybool(opr1, opr2 *Token, op string) (err error) {
	b1, err := cast.ToBoolE(opr1.value)
	if err != nil {
		return
	}
	b2, err := cast.ToBoolE(opr2.value)
	if err != nil {
		return
	}
	switch op {
	case "&&":
		opr1.value = b1 && b2
	case "||":
		opr1.value = b1 || b2
	default:
		return fmt.Errorf("bool-operator `%s` is not defined", op)
	}
	opr1.nature = nBool
	return
}

func binarycmp(opr1, opr2 *Token, op string) (err error) {
	if opr1.nature == nString || opr2.nature == nString {
		s1, err1 := cast.ToStringE(opr1.value)
		s2, err2 := cast.ToStringE(opr2.value)
		if err1 != nil {
			return err1
		}
		if err2 != nil {
			return err2
		}
		switch op {
		case "==":
			opr1.value = s1 == s2
		case "!=":
			opr1.value = s1 != s2
		case "<":
			opr1.value = s1 < s2
		case ">":
			opr1.value = s1 > s2
		case "<=":
			opr1.value = s1 <= s2
		case ">=":
			opr1.value = s1 >= s2
		default:
			return fmt.Errorf("cmp-s-operator `%s` is not defined", op)
		}
		opr1.nature = nBool
		return nil
	}
	if opr1.nature == nInt || opr2.nature == nInt {
		s1, err1 := cast.ToIntE(opr1.value)
		s2, err2 := cast.ToIntE(opr2.value)
		if err1 != nil {
			return err1
		}
		if err2 != nil {
			return err2
		}
		switch op {
		case "==":
			opr1.value = s1 == s2
		case "!=":
			opr1.value = s1 != s2
		case "<":
			opr1.value = s1 < s2
		case ">":
			opr1.value = s1 > s2
		case "<=":
			opr1.value = s1 <= s2
		case ">=":
			opr1.value = s1 >= s2
		default:
			return fmt.Errorf("cmp-i-operator `%s` is not defined", op)
		}
		opr1.nature = nBool
		return nil
	}
	if opr1.nature == nBool || opr2.nature == nBool {
		b1, err1 := cast.ToBoolE(opr1.value)
		b2, err2 := cast.ToBoolE(opr2.value)
		if err1 != nil {
			return err1
		}
		if err2 != nil {
			return err2
		}
		switch op {
		case "==":
			opr1.value = b1 == b2
		case "!=":
			opr1.value = b1 != b2
		case "<":
			opr1.value = !b1 && b2
		case ">":
			opr1.value = b1 && !b2
		case "<=":
			opr1.value = (b1 == b2) || (!b1 && b2)
		case ">=":
			opr1.value = (b1 == b2) || (b1 && !b2)
		default:
			return fmt.Errorf("cmp-b-operator `%s` is not defined", op)
		}
		opr1.nature = nBool
		return nil
	}

	if opr1.nature == nVar && opr2.nature == nVar {
		switch opr1.value.(type) {
		case string, []byte, []rune:
			opr1.nature = nString
			return binarycmp(opr1, opr2, op)
		case int64, int, int32, int8, uint, uint64, uint32, uint8:
			opr1.nature = nInt
			return binarycmp(opr1, opr2, op)
		case bool:
			opr1.nature = nBool
			return binarycmp(opr1, opr2, op)
		default:
			return fmt.Errorf("cmp-v-operator `%s` is not defined", op)
		}
	}

	return fmt.Errorf("cmp-a-operator `%s` is not defined", op)
}

func binaryconcat(opr1, opr2 *Token) (err error) {
	s1, err1 := cast.ToStringE(opr1.value)
	s2, err2 := cast.ToStringE(opr2.value)
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}

	opr1.value = s1 + s2
	opr1.nature = nString
	return nil
}

func binarytest(opr1, opr2 *Token, op string) (err error) {
	s1, err1 := cast.ToStringE(opr1.value)
	s2, err2 := cast.ToStringE(opr2.value)
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	switch op {
	case "_=_":
		opr1.value = strings.Contains(s2, s1)
	case "=_":
		opr1.value = strings.HasPrefix(s2, s1)
	case "_=":
		opr1.value = strings.HasSuffix(s2, s1)
	}
	return nil

}

func binarymatch(opr1, opr2 *Token) (err error) {
	s1, err1 := cast.ToStringE(opr1.value)
	if err1 != nil {
		return err1
	}
	rex := new(regexp.Regexp)

	switch s2 := (opr2.value).(type) {
	case regexp.Regexp:
		rex = &s2
	case *regexp.Regexp:
		rex = s2
	default:
		s, err := cast.ToStringE(s2)
		if err != nil {
			return err
		}
		rex, err = regexp.Compile(s)
		if err != nil {
			return err
		}
	}
	opr1.value = rex.MatchString(s1)
	return nil
}

func binaryact(opr1, opr2 *Token, placeholders []map[string]any) (err error) {
	if opr2.value == nil {
		return fmt.Errorf("value of operand is nil")
	}
	if opr1.value == nil {
		return fmt.Errorf("value of  operand is nil")
	}

	index := false
	fun := false
	rt := reflect.TypeOf(opr2.value)
	rk := rt.Kind()
	switch rk {
	case reflect.Slice, reflect.Array, reflect.Map:
		index = true
	case reflect.Func:
		fun = true
	}
	if !index && !fun && (opr1.stop || opr2.stop) {
		return fmt.Errorf("cannot apply operation on this pair")
	}
	if !index && !fun {
		copr1 := *opr1
		copr2 := *opr2
		copr1.stop = true
		copr2.stop = true
		err := binaryact(&copr1, &copr2, placeholders)
		if err != nil {
			return err
		}
		opr1.value = copr2.value
		opr1.nature = copr2.nature
		return nil
	}

	if index {
		inx, err := cast.ToInt64E(opr1.value)
		sam := reflect.ValueOf(opr2.value)
		if err == nil && inx >= 0 && inx <= math.MaxInt {
			v := int(inx)
			switch rk {
			case reflect.Slice, reflect.Array:
				if v < sam.Len() {
					opr1.value = sam.Index(v).Interface()
					opr1.nature = nVar
					return nil
				}
				return fmt.Errorf("integer index is greater than length")
			default:
				return fmt.Errorf("cannot apply operation on this integer")
			}
		}
		switch rk {
		case reflect.Map:
			if sam.Type().Key().Kind() != reflect.String {
				return fmt.Errorf("map should have string keys")
			}
			opr1.value = sam.MapIndex(reflect.ValueOf(opr1.value)).Interface()
			opr1.nature = nVar
			return nil
		default:
			return fmt.Errorf("map should have string keys")
		}
	}

	if fun {
		origin := opr2.origin
		fna, _ := Value(origin, placeholders)
		switch fn := fna.(type) {
		case func(any) (any, error):
			var err error
			opr1.value, err = fn(opr1.value)
			opr1.nature = nVar
			return err
		case func(any, ...any) (any, error):
			var err error
			opr1.value, err = fn(opr1.value)
			opr1.nature = nVar
			return err
		}
	}
	return fmt.Errorf("illegal operation on `%s` and `%s`", opr1.origin, opr2.origin)

}
