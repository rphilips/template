package expr

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

var escape = '\\'
var quote1 = '"'
var quote2 = '`'
var dollar = "$"

const (
	nUndef = iota
	nString
	nInt
	nBool
	nVar
	nOp
	nUnary
	nBinary
	nSpace
	nLParen
	nRParen
)

type Token struct {
	value  any
	nature rune
	origin string
	stop   bool
}

func (token Token) MarshalJSON() ([]byte, error) {
	m := make(map[string]any)
	m["nature"] = token.Nature()
	m["value"] = token.value
	m["origin"] = token.origin
	b, _ := json.MarshalIndent(m, "", "    ")
	return b, nil
}

func (token Token) Origin() string {
	return token.origin
}

func (token Token) Nature() string {
	switch token.nature {
	case nUndef:
		return "undefined"
	case nString:
		return "string"
	case nInt:
		return "integer"
	case nBool:
		return "bool"
	case nVar:
		return "var"
	case nUnary:
		return "unaryop"
	case nBinary:
		return "binaryop"
	case nSpace:
		return "space"
	case nLParen:
		return "("
	case nRParen:
		return ")"
	default:
		return ""
	}
}

type Postfix []Token

type stack []Token

// IsEmpty: check if stack is empty
func (st *stack) IsEmpty() bool {
	return len(*st) == 0
}

// Push a new value onto the stack
func (st *stack) Push(tok Token) {
	*st = append(*st, tok) //Simply append the new value to the end of the stack
}

// Remove top element of stack. Return false if stack is empty.
func (st *stack) Pop() bool {
	if st.IsEmpty() {
		return false
	} else {
		index := len(*st) - 1 // Get the index of top most element.
		*st = (*st)[:index]   // Remove it from the stack by slicing it off.
		return true
	}
}

// Return top element of stack.
func (st *stack) Top() (tok Token) {
	if st.IsEmpty() {
		return
	} else {
		index := len(*st) - 1   // Get the index of top most element.
		element := (*st)[index] // Index onto the slice and obtain the element.
		return element
	}
}

// Function to return precedence of operators
func prec(tok Token, unary bool) int {
	s := tok.value.(string)
	switch s {
	case "||":
		return 1
	case "&&":
		return 2
	case "==", "!=":
		return 4
	case "<", ">", "<=", ">=", "_=_", "~", "=_", "_=":
		return 5
	case "_":
		return 6
	case "+", "-":
		if unary {
			return 9
		}
		return 6
	case "*", "/", "%":
		return 7
	case "^":
		return 8
	case "!!", "!", "#":
		return 9
	case ".":
		return 10
	default:
		return -1
	}
}

func (postfix Postfix) Eval(placeholders []map[string]any) (value any, err error) {
	if len(postfix) == 0 {
		return nil, nil
	}

	var mystack stack

	for _, token := range postfix {
		nature := token.nature
		value := token.value
		origin := token.origin
		switch nature {
		case nInt, nString, nBool:
			mystack.Push(token)
		case nVar:
			val, err := Value(origin, placeholders)
			if err != nil {
				err = fmt.Errorf("unknown identifier `%s` in expression `%s`", token.origin, postfix.String())
				return nil, err
			}
			token.value = val
			mystack.Push(token)
		case nUnary:
			opr1 := mystack.Top()
			mystack.Pop()
			switch value {
			case "#":
				err = unarylen(&opr1)
			case "-":
				err = unaryminus(&opr1)
			case "+":
				err = unaryplus(&opr1)
			case "!":
				err = unarynegate(&opr1)
			case "!!":
				err = unarydoublenegate(&opr1)
			}
			if err != nil {
				return nil, err
			}
			mystack.Push(opr1)
		case nBinary:
			opr2 := mystack.Top()
			mystack.Pop()
			opr1 := mystack.Top()
			mystack.Pop()
			op := value.(string)
			switch op {
			case "^", "+", "-", "*", "/", "%":
				err = binarymath(&opr1, &opr2, op)
			case "==", "!=", "<", ">", "<=", ">=":
				err = binarycmp(&opr1, &opr2, op)
			case "&&", "||":
				err = binarybool(&opr1, &opr2, op)
			case "_":
				err = binaryconcat(&opr1, &opr2)
			case "_=_", "_=", "=_":
				err = binarytest(&opr1, &opr2, op)
			case "~":
				err = binarymatch(&opr1, &opr2)
			case ".":
				err = binaryact(&opr1, &opr2, placeholders)
				if err != nil {
					err = binaryact(&opr2, &opr1, placeholders)
					opr1 = opr2
				}
			}
			if err != nil {
				return nil, err
			}
			mystack.Push(opr1)
			continue
		}

	}
	return mystack.Top().value, nil
}

func Value(placeholder string, placeholders []map[string]any) (val any, err error) {
	origin := placeholder
	if strings.HasPrefix(placeholder, dollar) {
		placeholder = placeholder[1:]
	}
	for i := len(placeholders); i > 0; i-- {
		val, ok := placeholders[i-1][placeholder]
		if ok {
			return val, nil
		}
	}
	return nil, fmt.Errorf("unknown placeholder `%s`", origin)
}

func InfixToPostfix(infix string) (postfix Postfix, err error) {
	tokens, err := tokenize([]rune(infix))
	if err != nil {
		return
	}
	var sta stack
	for _, tok := range tokens {
		nature := tok.nature
		// if scanned character is operand, add it to output string
		switch nature {

		case nVar, nInt, nBool, nString:
			postfix = append(postfix, tok)
		case nLParen:
			sta.Push(tok)
		case nRParen:
			for sta.Top().nature != nLParen {
				postfix = append(postfix, sta.Top())
				sta.Pop()
			}
			sta.Pop()
		default:
			unary := nature == nUnary
			for !sta.IsEmpty() && prec(tok, unary) <= prec(sta.Top(), unary) {
				postfix = append(postfix, sta.Top())
				sta.Pop()
			}
			sta.Push(tok)
		}
	}

	// Pop all the remaining elements from the stack

	for !sta.IsEmpty() {
		postfix = append(postfix, sta.Top())
		sta.Pop()
	}
	counter := 0

	for _, tok := range postfix {
		nature := tok.nature
		switch nature {
		case nVar, nInt, nString:
			counter++
		case nUnary:
			counter--
			if counter < 0 {
				return nil, fmt.Errorf("wrong unary operation `%s` in `%s`", tok.value.(string), infix)
			}
			counter++
		case nBinary:
			counter--
			counter--
			if counter < 0 {
				return nil, fmt.Errorf("invalid binary `%s` expression `%s`", tok.value.(string), infix)
			}
			counter++
		}

	}
	if counter != 1 {
		return nil, fmt.Errorf("invalid expression `%s`", infix)
	}
	return postfix, nil
}

func PostfixToInfix(postfix Postfix) (string, error) {
	sta := stack(make([]Token, 0, len(postfix)))
	for _, token := range postfix {
		switch token.nature {
		case nUnary:
			if sta.IsEmpty() {
				return "", fmt.Errorf("invalid postfix notation")
			}
			op1 := sta.Top()
			sta.Pop()
			aux := "(" + op1.Origin() + token.Origin() + ")"
			sta.Push(Token{nature: nVar, origin: aux})
		case nBinary:
			if sta.IsEmpty() {
				return "", fmt.Errorf("invalid postfix notation")
			}
			op1 := sta.Top()
			sta.Pop()
			if sta.IsEmpty() {
				return "", fmt.Errorf("invalid postfix notation")
			}
			op2 := sta.Top()
			sta.Pop()
			aux := "(" + op2.Origin() + token.Origin() + op1.Origin() + ")"
			sta.Push(Token{nature: nVar, origin: aux})
		default:
			sta.Push(token)
		}

	}
	if sta.IsEmpty() {
		return "", fmt.Errorf("invalid postfix notation")
	}
	v := sta.Top().Origin()
	return parens(v), nil
}

func (postfix Postfix) String() string {
	s, err := PostfixToInfix(postfix)
	if err != nil {
		return "???"
	}
	return s
}
func parens(s string) string {
	if !strings.HasPrefix(s, "(") || !strings.HasSuffix(s, ")") {
		return s
	}
	v := strings.TrimPrefix(s, "(")
	v = strings.TrimSuffix(v, ")")
	if v == "" {
		return s
	}
	_, err := InfixToPostfix(v)
	if err != nil {
		return s
	}
	return parens(v)
}

func tokenize(rs []rune) (tokens []Token, err error) {
	start := 0
	for {
		t, pos, err := gobble(rs, start)
		start = pos + 1
		if err != nil {
			return nil, err
		}
		if t.nature == nUndef {
			break
		}
		if t.nature != nSpace {
			tokens = append(tokens, t)
		}
	}
	level := 0
	unary := true
	for i, t := range tokens {
		switch t.nature {
		case nBool, nInt, nString:
			unary = false
			continue
		case nLParen:
			level++
			unary = true
			continue
		case nRParen:
			level--
			unary = false
			if level < 0 {
				return nil, fmt.Errorf("too many `%s` in expression `%s`", t.value.(string), string(rs))
			}
			continue
		case nBinary:
			if i == len(tokens)-1 {
				return nil, fmt.Errorf("misplaced operator `%s` in expression `%s`", t.value.(string), string(rs))
			}
			if unary {
				tokens[i].nature = nUnary
			}
			switch t.value {
			case "/", "<", ">", "^", "==", "&&", "||", "!", "!!", "%", "*", "+", "-", ".", "!=", "<=", ">=", "#", "_", "_=_", "=_", "~", "_=":
				continue
			case "!<", "<!", "=>":
				tokens[i].value = ">="
			case "!>", ">!", "=<":
				tokens[i].value = "<="
			case "**":
				tokens[i].value = "^"
			case "=":
				tokens[i].value = "=="
			case "=!", "<>", "><":
				tokens[i].value = "!="
			default:
				return nil, fmt.Errorf("invalid operator `%s` found in expression `%s`", t.value.(string), string(rs))
			}
			unary = true
		case nVar:
			unary = false
			if t.value == "?" {
				continue
			}
			v := strings.TrimLeft(t.origin, "$")
			if v == "" {
				return nil, fmt.Errorf("invalid identifier `%s` found in expression `%s`", t.value.(string), string(rs))
			}
			w, _ := utf8.DecodeRuneInString(v)
			if w == utf8.RuneError {
				return nil, fmt.Errorf("not valid UTF-8 `%s` found in expression `%s`", t.value.(string), string(rs))
			}
			if !unicode.IsLetter(w) {
				return nil, fmt.Errorf("`%s` should start with a letter in expression `%s`", t.value.(string), string(rs))
			}
			for _, r := range v {
				if !unicode.IsLetter(r) && !unicode.IsNumber(r) {
					return nil, fmt.Errorf("`%s` should only consist of letters or numbers in expression `%s`", t.value.(string), string(rs))
				}
			}
		}
	}
	if level > 0 {
		return nil, fmt.Errorf("unmatched `(` in expression `%s`", string(rs))
	}

	return

}

func gobble(rs []rune, start int) (t Token, last int, err error) {

	t.value = ""
	t.nature = nUndef
	l := len(rs)
	if start >= l {
		return t, l, nil
	}
	c := rs[start]
	last = start
	if unicode.IsSpace(c) {
		t.nature = nSpace
		return
	}
	var b strings.Builder
	switch c {
	case quote1, quote2:
		if start+1 == l {
			err = fmt.Errorf("missing `%c` in expression `%s`", c, string(rs))
			return
		}
		b.WriteRune(c)
		escaped := false
		last = start
		for i := start + 1; i < l; i++ {
			last = i
			r := rs[i]
			b.WriteRune(r)
			if (c == quote2 && r == quote2) || (c == quote1 && r == quote1 && !escaped) {
				t.origin = b.String()
				t.value, err = strconv.Unquote(t.origin)
				t.nature = nString
				if err != nil {
					break
				}
				return
			}
			escaped = !escaped && r == escape
		}
		err = fmt.Errorf("malformed string %s in expression `%s`", t.origin, string(rs))
		return

	case '(', ')':
		t.origin = string(c)
		t.value = t.origin
		t.nature = nLParen
		if c == ')' {
			t.nature = nRParen
		}
		last = start

	case '-', '+', '*', '!', '&', '|', '<', '>', '=', '_', '#', '~', '/', '^', '.':
		for i := start; i < len(rs); i++ {
			r := rs[i]
			if strings.ContainsRune("+-*!&|<>=.#~/^_", r) {
				last = i
				b.WriteRune(r)
			} else {
				break
			}
		}
		t.origin = b.String()
		t.value = t.origin
		t.nature = nBinary
		return

	case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		for i := start; i < len(rs); i++ {
			r := rs[i]
			if strings.ContainsRune("1234567890", r) {
				last = i
				b.WriteRune(r)
			} else {
				break
			}
		}
		t.origin = b.String()
		t.value, _ = strconv.ParseInt(t.origin, 10, 64)
		t.nature = nInt
		return

	default:
		for i := start; i < len(rs); i++ {
			r := rs[i]
			if r == '_' {
				break
			}
			if (r == '$' && i == start) || unicode.IsLetter(r) || unicode.IsNumber(r) {
				last = i
				b.WriteRune(r)
			} else {
				break
			}
		}
		t.origin = b.String()
		t.value = t.origin
		t.nature = nVar
		if t.value == "" {
			err = fmt.Errorf("uninterpreted character `%c` in expression `%s`", c, string(rs))
			return
		}
		return
	}
	return
}

func Eval(infix string, placeholders []map[string]any) (result any, err error) {

	postfix, err := InfixToPostfix(infix)

	if err != nil {
		return
	}
	return postfix.Eval(placeholders)
}
