package expr

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func TestEvalv(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "v1",
			args: args{
				infix: `a`,
			},
			wantResult: "A",
		},
		{
			name: "v2",
			args: args{
				infix: `$a`,
			},
			wantResult: "A",
		},
		{
			name: "v3",
			args: args{
				infix: `123`,
			},
			wantResult: int64(123),
		},
		{
			name: "v4",
			args: args{
				infix: "`x\\ny`",
			},
			wantResult: `x\ny`,
		},
		{
			name: "v5",
			args: args{
				infix: `"x\ny"`,
			},
			wantResult: "x\ny",
		},
		{
			name: "v6",
			args: args{
				infix: `5`,
			},
			wantResult: int64(5),
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestEvalu(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "u1",
			args: args{
				infix: `-3`,
			},
			wantResult: int64(-3),
		},
		{
			name: "u2",
			args: args{
				infix: `+3`,
			},
			wantResult: int64(3),
		},
		{
			name: "u3",
			args: args{
				infix: `!true`,
			},
			wantResult: false,
		},
		{
			name: "u4",
			args: args{
				infix: `!false`,
			},
			wantResult: true,
		},
		{
			name: "u5",
			args: args{
				infix: `!!true`,
			},
			wantResult: true,
		},
		{
			name: "u6",
			args: args{
				infix: `!!false`,
			},
			wantResult: false,
		},
		{
			name: "u70",
			args: args{
				infix: `a`,
			},
			wantResult: "A",
		},
		{
			name: "u7",
			args: args{
				infix: `#a`,
			},
			wantResult: int64(1),
		},
		{
			name: "u8",
			args: args{
				infix: `#"abc"`,
			},
			wantResult: int64(3),
		},
		{
			name: "u9",
			args: args{
				infix: `#list`,
			},
			wantResult: int64(6),
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestEvalb(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "b1",
			args: args{
				infix: `5-3`,
			},
			wantResult: int64(2),
		},
		{
			name: "b2",
			args: args{
				infix: `5+3`,
			},
			wantResult: int64(8),
		},
		{
			name: "b3",
			args: args{
				infix: `5*3`,
			},
			wantResult: int64(15),
		},
		{
			name: "b4",
			args: args{
				infix: `13*3`,
			},
			wantResult: int64(39),
		},
		{
			name: "b5",
			args: args{
				infix: `13/3`,
			},
			wantResult: int64(4),
		},
		{
			name: "b6",
			args: args{
				infix: `2^3`,
			},
			wantResult: int64(8),
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalc(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "c1",
			args: args{
				infix: `3<15`,
			},
			wantResult: true,
		},
		{
			name: "c2",
			args: args{
				infix: `"3"<"15"`,
			},
			wantResult: false,
		},
		{
			name: "c3",
			args: args{
				infix: `3>15`,
			},
			wantResult: false,
		},
		{
			name: "c4",
			args: args{
				infix: `"3">"15"`,
			},
			wantResult: true,
		},
		{
			name: "c5",
			args: args{
				infix: `3==3`,
			},
			wantResult: true,
		},
		{
			name: "c6",
			args: args{
				infix: `"abc"=="abc"`,
			},
			wantResult: true,
		},
		{
			name: "c5",
			args: args{
				infix: `3<=3`,
			},
			wantResult: true,
		},
		{
			name: "c6",
			args: args{
				infix: `"abc"<="abc"`,
			},
			wantResult: true,
		},
		{
			name: "c7",
			args: args{
				infix: `3<=1`,
			},
			wantResult: false,
		},
		{
			name: "c8",
			args: args{
				infix: `"abc"<="Abc"`,
			},
			wantResult: false,
		},
		{
			name: "c9",
			args: args{
				infix: `3>=1`,
			},
			wantResult: true,
		},
		{
			name: "c10",
			args: args{
				infix: `"abc">="Abc"`,
			},
			wantResult: true,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalt(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "t1",
			args: args{
				infix: `true&&true`,
			},
			wantResult: true,
		},
		{
			name: "t2",
			args: args{
				infix: `true&&false`,
			},
			wantResult: false,
		},
		{
			name: "t3",
			args: args{
				infix: `false&&true`,
			},
			wantResult: false,
		},
		{
			name: "t4",
			args: args{
				infix: `false&&false`,
			},
			wantResult: false,
		},
		{
			name: "t5",
			args: args{
				infix: `true||true`,
			},
			wantResult: true,
		},
		{
			name: "t6",
			args: args{
				infix: `true||false`,
			},
			wantResult: true,
		},
		{
			name: "t7",
			args: args{
				infix: `false||true`,
			},
			wantResult: true,
		},
		{
			name: "t8",
			args: args{
				infix: `false||false`,
			},
			wantResult: false,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalcc(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "cc1",
			args: args{
				infix: `"abc"_"def"`,
			},
			wantResult: "abcdef",
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalst(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "st1",
			args: args{
				infix: `"abc"_=_"deabcf"`,
			},
			wantResult: true,
		},
		{
			name: "st2",
			args: args{
				infix: `"abc"_=_"deabf"`,
			},
			wantResult: false,
		},
		{
			name: "st3",
			args: args{
				infix: `"abc"=_"abcdef"`,
			},
			wantResult: true,
		},
		{
			name: "st4",
			args: args{
				infix: `"abc"_=_"abdef"`,
			},
			wantResult: false,
		},
		{
			name: "st5",
			args: args{
				infix: `"abc"_="defabc"`,
			},
			wantResult: true,
		},
		{
			name: "st6",
			args: args{
				infix: `"abc"_="defab"`,
			},
			wantResult: false,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalm(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	ph := []map[string]any{uph, lph}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "m1",
			args: args{
				infix: `"abc"~"[b]"`,
			},
			wantResult: true,
		},
		{
			name: "m2",
			args: args{
				infix: `"abc"~"[x]"`,
			},
			wantResult: false,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalfn(t *testing.T) {

	lph := make(map[string]any)
	uph := make(map[string]any)
	ph := []map[string]any{uph, lph}
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	lph["dict"] = map[string]string{"a": "Alfa", "b": "Beta"}
	lph["func"] = func(arg any) (any, error) {
		switch a := arg.(type) {
		case string:
			return strings.ToLower(a), nil
		default:
			return nil, fmt.Errorf("argument not of the right type")
		}
	}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "fn1",
			args: args{
				infix: `2.list`,
			},
			wantResult: "E",
		},
		{
			name: "fn2",
			args: args{
				infix: `"a".dict`,
			},
			wantResult: "Alfa",
		},
		{
			name: "fn3",
			args: args{
				infix: `"a".dict_"b".dict`,
			},
			wantResult: "AlfaBeta",
		},
		{
			name: "fn4",
			args: args{
				infix: `"ABC".func`,
			},
			wantResult: "abc",
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x, per := json.MarshalIndent(tpostfix, "", "    ")
			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\nerr: %s", err, tt.wantErr, string(x), per)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\nerr: %s", gotResult, tt.wantResult, string(x), per)
			}
		})
	}
}

func TestEvalcomb(t *testing.T) {
	lph := make(map[string]any)
	uph := make(map[string]any)
	ph := []map[string]any{uph, lph}
	uph["true"] = true
	uph["false"] = false
	lph["a"] = "A"
	lph["b"] = "B"
	lph["list"] = []string{"Q", "W", "E", "R", "T", "Y"}
	lph["dict"] = map[string]string{"a": "Alfa", "b": "Beta"}
	lph["func"] = func(arg any) (any, error) {
		switch a := arg.(type) {
		case string:
			return strings.ToLower(a), nil
		default:
			return nil, fmt.Errorf("argument not of the right type")
		}
	}
	type args struct {
		infix string
	}
	tests := []struct {
		name       string
		args       args
		wantResult any
		wantErr    bool
	}{
		{
			name: "comb1",
			args: args{
				infix: `1+2*3`,
			},
			wantResult: int64(7),
		},
		{
			name: "comb2",
			args: args{
				infix: `(1+2)*3`,
			},
			wantResult: int64(9),
		},
		{
			name: "comb3",
			args: args{
				infix: `((1+2)*3)*4`,
			},
			wantResult: int64(36),
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotResult, err := Eval(tt.args.infix, ph)
			if err != nil {
				t.Errorf(err.Error())
			}
			tpostfix, _ := InfixToPostfix(tt.args.infix)
			x := showPostfix(tpostfix)

			if (err != nil) != tt.wantErr {
				t.Errorf("Eval() error = %v, wantErr %v\npostfix: %s\n", gotResult, tt.wantErr, x)
				return
			}
			if !reflect.DeepEqual(gotResult, tt.wantResult) {
				t.Errorf("Eval() = %v, want %v\npostfix: %s\n", gotResult, tt.wantResult, x)
			}
		})
	}
}

func showPostfix(postfix []Token) string {
	s := "["
	for _, t := range postfix {
		b, _ := json.Marshal(t)
		s += "\n" + string(b)
	}
	s += "\n]"
	return s
}

func TestPostfixToInfix(t *testing.T) {
	type test struct {
		s        string
		expected string
	}
	tests := []test{
		{
			s:        "ABC",
			expected: "ABC",
		},
		{
			s:        "ABC+123",
			expected: "ABC+123",
		},
		{
			s:        "(ABC+123)*67",
			expected: "(ABC+123)*67",
		},
		{
			s:        "A+B+C",
			expected: "(A+B)+C",
		},
	}
	for _, tt := range tests {
		postfix, _ := InfixToPostfix(tt.s)
		calc, err := PostfixToInfix(postfix)
		if err != nil {
			t.Errorf("Eval() = %s, error : %s\n", tt.s, err)
		}
		if err == nil && calc != tt.expected {
			t.Errorf("Eval() = %s, expected : %s\nfound: [%s] > %s", tt.s, tt.expected, calc, parens(calc))
		}
	}
}
