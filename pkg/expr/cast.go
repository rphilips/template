package expr

import (
	"strconv"
	"strings"

	"github.com/spf13/cast"
)

func ToBool(value any) (b bool, err error) {
	if value == nil {
		return false, nil
	}
	switch val := value.(type) {
	case bool:
		return val, nil
	default:
		v, err := ToInt64(value)
		if err == nil {
			return v != 0, nil
		}
		return cast.ToBoolE(value)
	}
}

func ToInt64(value any) (i int64, err error) {
	if value == nil {
		return 0, nil
	}
	i, err = cast.ToInt64E(value)
	if err == nil {
		return i, nil
	}
	s, err := cast.ToStringE(value)
	if err != nil {
		return 0, err
	}
	minus := false
	if !strings.HasPrefix(s, "+") {
		if strings.HasPrefix(s, "-") {
			minus = true
			s = s[1:]
		}
	} else {
		s = s[1:]
	}
	if s == "" {
		return 0, nil
	}

	x := strings.TrimLeft(s, "1234567890")
	s = strings.TrimSuffix(s, x)
	if s == "" {
		return 0, nil
	}
	i, _ = strconv.ParseInt(s, 10, 64)
	if minus {
		return -i, nil
	} else {
		return i, nil
	}
}

func ToString(value any) (s string, err error) {
	if value == nil {
		return "", nil
	}
	switch v := value.(type) {
	case rune:
		return string(v), nil
	default:
		return cast.ToStringE(value)
	}
}
