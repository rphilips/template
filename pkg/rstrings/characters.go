package rstrings

import (
	"unicode/utf8"
)

// Vars

var ASCIILowercase = "abcdefghijklmnopqrstuvwxyz"
var ASCIIUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
var ASCIILetters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
var Digits = "0123456789"
var HexDigits = "0123456789abcdefABCDEF"
var OctDigits = "01234567"
var Punctuation = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~."
var ASCIIWhitespace = " \t\n\r\x0b\x0c"
var Whitespace = "\u0009\u000A\u000B\u000C\u000D\u0020\u0085\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u2028\u2029\u202F\u205F\u3000"

// IsASCII returns true if the entire string contains only ASCII characters.
// On empty string, the function returns true.
func IsASCII(str string) bool {
	for _, ch := range str {
		if ch > 127 {
			return false
		}
	}

	return true
}

// IsANSI returns true if the entire string contains only ANSI characters.
// On empty string, the function returns true.
func IsANSI(str string) bool {
	for _, ch := range str {
		if ch > 255 {
			return false
		}
	}

	return true
}

// IsNumber returns true if the entire string contains only ASCII digits
// On empty string, the function returns false
func IsNumber(str string) bool {
	if str == "" {
		return false
	}
	for _, ch := range str {
		if ch > 57 {
			return false
		}
		if ch < 48 {
			return false
		}
	}

	return true
}

// ValidUTF8 reports whether string consists entirely of valid UTF-8-encoded runes.
var ValidUTF8 = utf8.Valid
