package rstrings

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"strings"
	"unicode/utf8"

	"github.com/huandu/xstrings"
)

// TrimSpace returns a slice of the string s, with all leading
// and trailing white space removed, as defined by Unicode.
var TrimSpace = strings.TrimSpace

// Center
// [xstrings.Center]
var Center = xstrings.Center

// Count
// [xstrings.Count]
var Count = xstrings.Count

// Delete
// [xstrings.Delete]
var Delete = xstrings.Delete

// ExpandTabs
// [xstrings.ExpandTabs]
var ExpandTabs = xstrings.ExpandTabs

// FirstRuneToLower
// [xstrings.FirstRuneToLower]
var FirstRuneToLower = xstrings.FirstRuneToLower

// FirstRuneToUpper
// [xstrings.FirstRuneToUpper]
var FirstRuneToUpper = xstrings.FirstRuneToUpper

// Insert
// [xstrings.Insert]
var Insert = xstrings.Insert

// PartitionLast
// [xstrings.LastPartition]
var PartitionLast = xstrings.LastPartition

// JustifyLeft
// [xstrings.LeftJustify]
var JustifyLeft = xstrings.LeftJustify

// Len
// [xstrings.Len]
var Len = xstrings.Len

// Partition
// [xstrings.Partition]
var Partition = xstrings.Partition

// Reverse
// [xstrings.Reverse]
var Reverse = xstrings.Reverse

// JustifyRight
// [xstrings.RightJustify]
var JustifyRight = xstrings.RightJustify

// RuneWidth
// [xstrings.RuneWidth]
var RuneWidth = xstrings.RuneWidth

// Scrub
// [xstrings.Scrub]
var Scrub = xstrings.Scrub

// Shuffle
// [xstrings.Shuffle]
var Shuffle = xstrings.Shuffle

// ShuffleSourc
// [xstrings.ShuffleSource]
var ShuffleSource = xstrings.ShuffleSource

// Slice
// [xstrings.Slice]
var Slice = xstrings.Slice

// Squeeze
// [xstrings.Squeeze]
var Squeeze = xstrings.Squeeze

// Successor
// [xstrings.Successor]
var Successor = xstrings.Successor

// SwapCase
// [xstrings.SwapCase]
var SwapCase = xstrings.SwapCase

// ToCamelCase
// [xstrings.ToCamelCase]
var ToCamelCase = xstrings.ToCamelCase

// ToKebabCase
// [xstrings.ToKebabCase]
var ToKebabCase = xstrings.ToKebabCase

// ToSnakeCase
// [xstrings.ToSnakeCase]
var ToSnakeCase = xstrings.ToSnakeCase

// Translate
// [xstrings.Translate]
var Translate = xstrings.Translate

// Width
// [xstrings.Width]
var Width = xstrings.Width

// WordCount
// [xstrings.WordCount]
var WordCount = xstrings.WordCount

// [xstrings.WordSplit]
// [xstrings.WordSplit]
var WordSplit = xstrings.WordSplit

// LowerLeft
// [xstrings.FirstRuneToLower]
var LowerLeft = xstrings.FirstRuneToLower

// UpperLeft
// [xstrings.FirstRuneToUpper]
var UpperLeft = xstrings.FirstRuneToUpper

// TrimSpaceLeft returns a slice of the string s, with all leading
// white space removed, whitespace as defined by Unicode.
func TrimSpaceLeft(s string) string {
	if s == "" {
		return ""
	}
	b := s[0]
	if b < 127 && b > 32 {
		return s
	}
	x := strings.TrimSpace(s)
	if x == "" {
		return ""
	}
	r, _ := utf8.DecodeRuneInString(x)
	if r == utf8.RuneError {
		return s
	}
	k := strings.IndexRune(s, r)
	return s[k:]
}

// TrimSpaceRight returns a slice of the string s, with all trailing white space removed,
// whitespace as defined by Unicode.
func TrimSpaceRight(s string) string {
	if s == "" {
		return ""
	}
	x := strings.TrimSpace(s)
	if x == "" {
		return ""
	}
	r, size := utf8.DecodeLastRuneInString(x)
	k := strings.LastIndexAny(s, string(r))
	return s[:k+size]
}

// ParseBrackets returns information on matching brackets in a string
// Arguments:
//
//	s: string to examine
//	lr: left bracket (rune) e.g. '('
//	rr: right bracket (rune) e.g. ')'
//
// ParseBrackets can be escaped. The default escape rune is '\'.
// change it by using the `WithEscapeRune` option.
// Turn of escaping with the `WithNotEscaped` option.
//
// The function returns a slice of integer slices of length 2:
// each of the consecutive elements matches a left bracket:
// its first element is the position of the left bracket in de string
// its second element is the position of the corresponding right element
//
// The function returns a boolean as well: 'true' means the string is well balanced.
// If false the resulting slice is nil
//

// Options:
//
//	WithNotEscaped: `lr` and `rr` are not escaped
//	WithEscapeRune('...'): replace the default escape rune with another rune
//
// Examples:
//
//	ParseBrackets("a(b(c))", '(', ')') -> [[1 6] [3 5]], true
//	ParseBrackets("é(b(é))", '(', ')') -> [[2 8] [4 7]], true
//	ParseBrackets("aébécèè", 'é', 'è') -> [[1 9] [4 7]], true
//	ParseBrackets(`(bb\)c\(dd)e`, '(', ')') -> [[1 11]], true
//	ParseBrackets(`Ë(bb\)c\(dd)e`, '(', ')') -> [[2 12]], true
//	ParseBrackets(`Ë(bb\)c\(dd)e`, '(', ')', WithNotEscaped) -> [[2 6], [9, 12]], true
func ParseBrackets(s string, lr rune, rr rune, options ...Option) (result [][]int, balanced bool) {
	if s == "" {
		return nil, true
	}
	config := NewCfg(options)
	escaper := config.escape
	notescaped := config.notescaped
	i := 0
	escaped := false
	b := make(map[int]int)
	level := 1
	size := 0
	for _, r := range s {
		i += size + 1
		size = 0
		if r > 127 {
			size = utf8.RuneLen(r) - 1
		}
		if notescaped {
			switch r {
			case lr:
				level++
				if b[level] != 0 {
					return nil, false
				}
				result = append(result, []int{i - 1, 0})
				b[level] = len(result)
			case rr:
				j := b[level]
				if j == 0 {
					return nil, false
				}
				result[j-1][1] = i - 1
				b[level] = 0
				level--
			}
		} else {
			switch r {
			case escaper:
				escaped = !escaped
			case lr:
				if escaped {
					escaped = false
					continue
				}
				level++
				if b[level] != 0 {
					return nil, false
				}
				result = append(result, []int{i - 1, 0})
				b[level] = len(result)
			case rr:
				if escaped {
					escaped = false
					continue
				}
				j := b[level]
				if j == 0 {
					return nil, false
				}
				result[j-1][1] = i - 1
				b[level] = 0
				level--
			default:
				escaped = false

			}
		}
	}
	if level != 1 {
		return nil, false
	}
	return result, true

}

// Split splits a string on a rune to a slice of strings.
// Options:
//
//	WithNotEscaped: true | false (default): indicates if the rune can be escaped to prevent splitting.
//	WithEscapeRune: escape character. Default '\'.
//	                The escape character is only special before the rune and the escape character itself.
//	WithKeepEscape: true | false (default): indicates if the escape characters are to be present after splitting
func Split(s string, r rune, options ...Option) []string {
	config := NewCfg(options)
	escaper := config.escape
	notescaped := config.notescaped
	keepescape := config.keepescape
	escaped := false
	if notescaped {
		return strings.SplitN(s, string(r), -1)
	}
	count := strings.Count(s, string(r))
	if count == 0 {
		return []string{s}
	}
	result := make([]string, 0, count+1)
	result = append(result, "")
	for _, ru := range s {
		switch ru {
		case escaper:
			if escaped {
				if keepescape {
					result[len(result)-1] += string(ru) + string(ru)
				} else {
					result[len(result)-1] += string(ru)
				}
				escaped = false
			} else {
				escaped = true
			}
		case r:
			if escaped {
				if keepescape {
					result[len(result)-1] += string(escaper) + string(ru)
				} else {
					result[len(result)-1] += string(ru)
				}
			} else {
				result = append(result, "")
			}
			escaped = false
		default:
			if escaped {
				result[len(result)-1] += string(escaper)
			}
			result[len(result)-1] += string(ru)
			escaped = false
		}
	}
	if escaped {
		result[len(result)-1] += string(escaper)
	}
	return result
}

// UnEscape replaces the escape rune, followed by a rune out of a specific set (escapeset), with the latter.
// If the only rune in the set is the escape character, it is af every rune belongs to the escapeset.
// (the escape rune is always added to this set)
// If the escape character is not followed by any rune (at the end of the string),
// it is added to the result
//
// Examples:
//
//	UnEscape(`a\b`, `b`, '\\') -> `ab`
//	UnEscape(`a\b`, `c`, '\\') -> `a\b`
//	UnEscape(`a\\b`, `c`, '\\') -> `a\b`
//	UnEscape(`ab\`, `c`, '\\') -> `ab\`
//	UnEscape(`ab\\`, `c`, '\\') -> `ab\`
//	UnEscape(`a\\b\\`, `\`, '\\') -> `ab\`
func UnEscape(s string, escapeset string, escape rune) string {
	all := escapeset == string(escape)
	if !all {
		escapeset += string(escape)
	}
	b := bytes.NewBuffer(nil)
	anyway := false
	for _, r := range s {
		if !anyway && r == escape {
			anyway = true
			continue
		}
		if anyway && !all && !strings.ContainsRune(escapeset, r) {
			b.WriteRune(escape)
		}
		b.WriteRune(r)
		anyway = false
	}
	if anyway {
		b.WriteRune(escape)
	}
	return b.String()
}

// Digest return a digest of a string
// It is based on the hexadecimal (lowercase) representation of
// SHA-256.
// Specify the option WithMaxLen to shorten the 64 byte representation
func Digest(s string, options ...Option) string {
	config := NewCfg(options)
	sha256 := sha256.Sum256([]byte(s))
	h := fmt.Sprintf("%x", sha256)
	if config.maxLen > 0 {
		max := min(64, config.maxLen)
		h = h[:max]
	}
	return h
}

// Translate str with the characters defined in from replaced by characters defined in to.
//
// From and to are patterns representing a set of characters. Pattern is defined as following.
//
// Special characters:
//
//  1. '-' means a range of runes, e.g.
//     "a-z" means all characters from 'a' to 'z' inclusive;
//     "z-a" means all characters from 'z' to 'a' inclusive.
//  2. '^' as first character means a set of all runes excepted listed, e.g.
//     "^a-z" means all characters except 'a' to 'z' inclusive.
//  3. '\' escapes special characters.
//
// Normal character represents itself, e.g. "abc" is a set including 'a', 'b' and 'c'.
//
// Translate will try to find a 1:1 mapping from from to to.
// If to is smaller than from, last rune in to will be used to map "out of range" characters in from.
//
// Note that '^' only works in the from pattern. It will be considered as a normal character in the to pattern.
//
// If the to pattern is an empty string, Translate works exactly the same as Delete.
//
// Samples:
//
//	Translate("hello", "aeiou", "12345")    => "h2ll4"
//	Translate("hello", "a-z", "A-Z")        => "HELLO"
//	Translate("hello", "z-a", "a-z")        => "svool"
//	Translate("hello", "aeiou", "*")        => "h*ll*"
//	Translate("hello", "^l", "*")           => "**ll*"
//	Translate("hello ^ world", `\^lo`, "*") => "he*** * w*r*d"

// Delete runes in str matching the pattern.
// Pattern is defined in Translate function.

// Reverse a utf8 encoded string.

func SubString(s string, i ...int) string {
	if len(s) == 0 {
		return s
	}
	if len(i) == 0 {
		return s
	}
	rs := []rune(s)

	length := len(rs)
	a := i[0]
	b := length
	if len(i) > 1 {
		b = i[1]
	}
	if a < 0 {
		a += length
	}
	if b < 0 {
		b += length
	}
	if a >= length {
		return ""
	}

	if b >= length {
		return string(rs[a:])
	}
	if a >= b {
		return ""
	}
	return string(rs[a:b])
}

func Fields(s string, sep string, joiner string, i ...int) string {
	if len(s) == 0 {
		return s
	}
	if len(i) == 0 {
		return s
	}
	var fields []string

	if sep == "" {
		fields = strings.Fields(s)
	} else {
		fields = strings.SplitN(s, sep, -1)
	}

	length := len(fields)
	a := i[0]
	b := length
	if len(i) > 1 {
		b = i[1]
	}
	if a < 0 {
		a += length
	}
	if b < 0 {
		b += length
	}
	if a >= length {
		return ""
	}
	if b >= length {
		return strings.Join(fields[a:], joiner)
	}
	if a >= b {
		return ""
	}
	return strings.Join(fields[a:b], joiner)
}

func NewTranslator(pattern string) *xstrings.Translator {
	return xstrings.NewTranslator(pattern, "")
}
