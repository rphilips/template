package rstrings

import (
	"testing"
)

func TestLeftTrimSpace(t *testing.T) {
	type test struct {
		s string
		e string
	}

	var r rune = 160

	tests := []test{
		{
			s: " ",
			e: "",
		},
		{
			s: " x",
			e: "x",
		},
		{
			s: " x ",
			e: "x ",
		},
		{
			s: string(r) + " x ",
			e: "x ",
		},
	}
	for _, tst := range tests {
		c := TrimSpaceLeft(tst.s)
		if c != tst.e {
			t.Errorf("found: `%s`\ntest: %v\n", c, tst)
		}
	}
}

func TestRightTrimSpace(t *testing.T) {
	type test struct {
		s string
		e string
	}

	var r rune = 160

	tests := []test{
		{
			s: " ",
			e: "",
		},
		{
			s: "x ",
			e: "x",
		},
		{
			s: " x ",
			e: " x",
		},
		{
			s: " x " + string(r),
			e: " x",
		},
	}
	for _, tst := range tests {
		c := TrimSpaceRight(tst.s)
		if c != tst.e {
			t.Errorf("found: `%s`\ntest: %v\n", c, tst)
		}
	}
}

func TestLeftRuneToUpper(t *testing.T) {
	type test struct {
		s string
		e string
	}

	var r rune = 235
	var ru rune = 203
	tests := []test{
		{
			s: "",
			e: "",
		},
		{
			s: "hello",
			e: "Hello",
		},
		{
			s: " hello",
			e: " hello",
		},
		{
			s: string(r) + "hello",
			e: string(ru) + "hello",
		},
	}
	for _, tst := range tests {
		c := UpperLeft(tst.s)
		if c != tst.e {
			t.Errorf("found: `%s`\ntest: %v\n", c, tst)
		}
	}
}

func TestLeftRuneToLower(t *testing.T) {
	type test struct {
		s string
		e string
	}

	var r rune = 235
	var ru rune = 203
	tests := []test{
		{
			s: "",
			e: "",
		},
		{
			s: "Hello",
			e: "hello",
		},
		{
			s: " hello",
			e: " hello",
		},
		{
			s: string(ru) + "hello",
			e: string(r) + "hello",
		},
	}
	for _, tst := range tests {
		c := LowerLeft(tst.s)
		if c != tst.e {
			t.Errorf("found: `%s`\ntest: %v\n", c, tst)
		}
	}
}

func TestBracket01(t *testing.T) {

	s := `a(bb)c`
	result, balanced := ParseBrackets(s, '(', ')')
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 1 {
		t.Errorf("length should be one")
	}
	if result[0][0] != 1 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 4 {
		t.Errorf("right bracket is on %d", result[0][1])
	}

	s = `a(bb(ddd))c`
	result, balanced = ParseBrackets(s, '(', ')')
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 2 {
		t.Errorf("length should be one")
	}
	if result[0][0] != 1 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 9 {
		t.Errorf("right bracket is on %d", result[0][1])
	}
	if result[1][0] != 4 {
		t.Errorf("left bracket is on %d", result[1][0])
	}
	if result[1][1] != 8 {
		t.Errorf("right bracket is on %d", result[1][1])
	}

	s = `a(bb)c(dd)e`
	result, balanced = ParseBrackets(s, '(', ')')
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 2 {
		t.Errorf("length should be one")
	}
	if result[0][0] != 1 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 4 {
		t.Errorf("right bracket is on %d", result[0][1])
	}
	if result[1][0] != 6 {
		t.Errorf("left bracket is on %d", result[1][0])
	}
	if result[1][1] != 9 {
		t.Errorf("right bracket is on %d", result[1][1])
	}

	s = `a(bb\)c\(dd)e`
	result, balanced = ParseBrackets(s, '(', ')')
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 1 {
		t.Errorf("length should be one")
	}
	if result[0][0] != 1 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 11 {
		t.Errorf("right bracket is on %d", result[0][1])
	}

	s = `Ë(bb\)c\(dd)e`
	result, balanced = ParseBrackets(s, '(', ')')
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 1 {
		t.Errorf("length should be one")
	}
	if result[0][0] != 2 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 12 {
		t.Errorf("right bracket is on %d", result[0][1])
	}

	s = `Ë(bb\)c\(dd)e`
	result, balanced = ParseBrackets(s, '(', ')', WithNotEscaped)
	if !balanced {
		t.Errorf("should be balanced")
	}
	if len(result) != 2 {
		t.Errorf("length should be two")
	}
	if result[0][0] != 2 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[0][1] != 6 {
		t.Errorf("right bracket is on %d", result[0][1])
	}
	if result[1][0] != 9 {
		t.Errorf("left bracket is on %d", result[0][0])
	}
	if result[1][1] != 12 {
		t.Errorf("right bracket is on %d", result[0][1])
	}

}

func TestUnescape01(t *testing.T) {
	if UnEscape(`a\b`, `b`, '\\') != `ab` {
		t.Errorf("error")
	}
	if UnEscape(`a\b`, `c`, '\\') != `a\b` {
		t.Errorf("error")
	}
	if UnEscape(`a\\b`, `c`, '\\') != `a\b` {
		t.Errorf("error")
	}
	if UnEscape(`ab\`, `c`, '\\') != `ab\` {
		t.Errorf("error")
	}
	if UnEscape(`ab\\`, `c`, '\\') != `ab\` {
		t.Errorf("error")
	}
	if UnEscape(`a\\b\\\c`, `\`, '\\') != `a\b\c` {
		t.Errorf("error")
	}
}

func TestSplit(t *testing.T) {
	type T struct {
		s string
		f []string
	}
	tests := []T{
		{
			s: "",
			f: []string{""},
		},
		{
			s: "a,b",
			f: []string{"a", "b"},
		},
		{
			s: "a,",
			f: []string{"a", ""},
		},
		{
			s: "a",
			f: []string{"a"},
		},
		{
			s: ",a",
			f: []string{"", "a"},
		},
		{
			s: "a,,b",
			f: []string{"a", "", "b"},
		},
		{
			s: `a\,b`,
			f: []string{"a,b"},
		},
		{
			s: `a,\,,b`,
			f: []string{"a", ",", "b"},
		},
		{
			s: `\a,b`,
			f: []string{`\a`, "b"},
		},
	}

	for _, test := range tests {
		f := Split(test.s, ',')
		if len(f) != len(test.f) {
			t.Errorf("error in `%s`: `%v`", test.s, f)
		}

		for i, p := range f {
			if p != test.f[i] {
				t.Errorf("error in `%s` (part %d): `%v`", test.s, i, f)
			}
		}
	}
}
