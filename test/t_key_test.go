package test

import (
	"testing"

	. "template"
)

func TestKey(t *testing.T) {

	tmpl, err := Compile(`A | key true "zzzza" | suffix true zzzza`)
	if err != nil {
		t.Errorf("error in compiling template = \n%s\n%s\n", tmpl, err)
	}

	s, err := tmpl.Eval(nil)
	if err != nil {
		t.Errorf("error in eval template = \n%s\n%s\n", tmpl, err)
	}

	if s != "AA" {
		t.Errorf("key did not work: %s", s)
	}
}
