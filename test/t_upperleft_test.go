package test

import (
	. "template"
	"testing"
)

func TestUpperLeft(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: "",
			r: "",
		},
		{
			t: "Hello World | upperLeft",
			r: "Hello World",
		},
		{
			t: "Hello World | upperLeft true",
			r: "Hello World",
		},
		{
			t: "hello World | upperLeft true",
			r: "Hello World",
		},
		{
			t: "Hello World | upperLeft 1=1",
			r: "Hello World",
		},
		{
			t: "Hello World | upperLeft false",
			r: "Hello World",
		},
		{
			t: "Hello World | upperLeft 1=0",
			r: "Hello World",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
