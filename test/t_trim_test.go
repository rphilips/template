package test

import (
	. "template"
	"testing"
)

func TestTrim(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: `Hello World | trim true "Held"`,
			r: "o Wor",
		},
		{
			t: `Hello World | trim true "A-Za-l"`,
			r: "o Wor",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
