package test

import (
	. "template"
	"testing"
)

func TestSubstring(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{

		{
			t: "élève | subString true 0",
			r: "élève",
		},
		{
			t: "élève | subString true 1",
			r: "lève",
		},
		{
			t: "élève | subString true 0",
			r: "élève",
		},
		{
			t: "élève | subString true 0 10000",
			r: "élève",
		},
		{
			t: "élève | subString true -1",
			r: "e",
		},
		{
			t: "élève | subString true -3 -1",
			r: "èv",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
