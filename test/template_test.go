package test

import (
	"testing"

	. "template"
)

func TestTemplate1(t *testing.T) {

	tmpl, err := Compile(`A | setID true "myID"`)
	if err != nil {
		t.Errorf("error in compiling template = \n%s\n%s\n", tmpl, err)
	}

	_, err = tmpl.Eval(nil)
	if err != nil {
		t.Errorf("error in eval template = \n%s\n%s\n", tmpl, err)
	}

	if tmpl.GetID() != "myID" {
		t.Errorf("not the right ID = \n%s\n", tmpl.GetID())
	}
}

func TestTemplate2(t *testing.T) {

	tmpl, err := Compile(`A | suffix true "B" | quit true | suffix true "C"`)
	if err != nil {
		t.Errorf("error in compiling template = \n%s\n%s\n", tmpl, err)
	}

	s, err := tmpl.Eval(nil)
	if err != nil {
		t.Errorf("error in eval template = \n%s\n%s\n", tmpl, err)
	}

	if s != "AB" {
		t.Errorf("not the right value: %s", s)
	}
}
