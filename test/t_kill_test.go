package test

import (
	. "template"
	"testing"
)

func TestKill(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: "Bonjour <élève> | kill",
			r: "",
		},
		{
			t: `Bonjour <élève> | setID "myID"`,
			r: "Bonjour <élève>",
		},
		{
			t: `Bonjour {<élève> | setID "myO"} | setID "myID"`,
			r: "Bonjour <élève>",
		},
		{
			t: "A{B{C | kill}}",
			r: "AB",
		},

		{
			t: "A{B{C | kill true `kid`}}",
			r: "ABC",
		},
		{
			t: "A{B{C | kill true `kid`} | setID true `kid`}",
			r: "A",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
