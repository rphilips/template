package test

import (
	. "template"
	"testing"
)

func TestReplace(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: `Hello World | replace true "Wo"`,
			r: "Hello rld",
		},
		{
			t: `Hello World | replace true "Wo" "XYZ"`,
			r: "Hello XYZrld",
		},
		{
			t: `Hello World | replace true "o" "O" 1`,
			r: "HellO World",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
