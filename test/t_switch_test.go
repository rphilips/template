package test

import (
	"testing"

	. "template"
)

func TestSwitch(t *testing.T) {
	tmpl, err := Compile(`A | key true "zzzza" | suffix true "BBB" | switch true "zzzza" `)
	if err != nil {
		t.Errorf("error in compiling template = \n%s\n%s\n", tmpl, err)
	}

	s, err := tmpl.Eval(nil)
	if err != nil {
		t.Errorf("error in eval template = \n%s\n%s\n", tmpl, err)
	}

	if s != "A" {
		t.Errorf("key did not work: %s", s)
	}
}
