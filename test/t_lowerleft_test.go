package test

import (
	. "template"
	"testing"
)

func TestLowerLeft(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: "",
			r: "",
		},
		{
			t: "Hello World | lowerLeft",
			r: "hello World",
		},
		{
			t: "Hello World | lowerLeft true",
			r: "hello World",
		},
		{
			t: "hello World | lowerLeft true",
			r: "hello World",
		},
		{
			t: "Hello World | lowerLeft 1=1",
			r: "hello World",
		},
		{
			t: "Hello World | lowerLeft false",
			r: "Hello World",
		},
		{
			t: "Hello World | lowerLeft 1=0",
			r: "Hello World",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
