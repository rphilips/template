package test

import (
	"testing"

	. "template"
)

func Test_SplitTk(t *testing.T) {
	type args struct {
		s      string
		offset int
	}
	tests := []struct {
		name       string
		args       args
		wantPbegin int
		wantPend   int
		wantPrest  int
		wantErr    bool
	}{
		{
			name: "tk1",
			args: args{
				s:      "Hello World",
				offset: 0,
			},
			wantPbegin: 0,
			wantPend:   0,
			wantPrest:  11,
			wantErr:    false,
		},
		{
			name: "tk2",
			args: args{
				s:      "",
				offset: 0,
			},
			wantPbegin: 0,
			wantPend:   0,
			wantPrest:  0,
			wantErr:    false,
		},
		{
			name: "tk3",
			args: args{
				s:      "Hello $World",
				offset: 0,
			},
			wantPbegin: 6,
			wantPend:   11,
			wantPrest:  12,
			wantErr:    false,
		},
		{
			name: "tk31",
			args: args{
				s:      "Hello $Worldé",
				offset: 0,
			},
			wantPbegin: 6,
			wantPend:   13,
			wantPrest:  14,
			wantErr:    false,
		},
		{
			name: "tk4",
			args: args{
				s:      "Hello $World,",
				offset: 0,
			},
			wantPbegin: 6,
			wantPend:   11,
			wantPrest:  12,
			wantErr:    false,
		},
		{
			name: "tk5",
			args: args{
				s:      "Hello \\$$World,",
				offset: 0,
			},
			wantPbegin: 8,
			wantPend:   13,
			wantPrest:  14,
			wantErr:    false,
		},

		// TODO: Add test cases.
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPbegin, gotPend, gotPrest, err := SplitP(tt.args.s, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("SplitTk() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotPbegin != tt.wantPbegin {
				t.Errorf("SplitTk() gotPbegin = %v, want %v", gotPbegin, tt.wantPbegin)
			}
			if gotPend != tt.wantPend {
				t.Errorf("SplitTk() gotPend = %v, want %v", gotPend, tt.wantPend)
			}
			if gotPrest != tt.wantPrest {
				t.Errorf("SplitTk() gotPrest = %v, want %v", gotPrest, tt.wantPrest)
			}
		})
	}
}

func Test_SplitTmpl(t *testing.T) {
	type args struct {
		s      string
		offset int
	}
	tests := []struct {
		name       string
		args       args
		wantTbegin int
		wantTend   int
		wantTrest  int
		wantErr    bool
	}{
		{
			name: "tmpl1",
			args: args{
				s:      "{}",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   1,
			wantTrest:  2,
			wantErr:    false,
		},
		{
			name: "tmpl2",
			args: args{
				s:      "{abc}",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   4,
			wantTrest:  5,
			wantErr:    false,
		},
		{
			name: "tmpl3",
			args: args{
				s:      "{abc`{}`}",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   8,
			wantTrest:  9,
			wantErr:    false,
		},
		{
			name: "tmpl4",
			args: args{
				s:      "{ab\\`c`{}`}",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   10,
			wantTrest:  11,
			wantErr:    false,
		},
		{
			name: "tmpl5",
			args: args{
				s:      "{ab\\`c`{}`",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   0,
			wantTrest:  0,
			wantErr:    true,
		},
		{
			name: "tmpl6",
			args: args{
				s:      "{}ab",
				offset: 0,
			},
			wantTbegin: 0,
			wantTend:   1,
			wantTrest:  2,
			wantErr:    false,
		},
		{
			name: "tmpl7",
			args: args{
				s:      "A{abc}",
				offset: 0,
			},
			wantTbegin: 1,
			wantTend:   5,
			wantTrest:  6,
			wantErr:    false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTbegin, gotTend, gotTrest, err := SplitT(tt.args.s, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("SplitTmpl() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotTbegin != tt.wantTbegin {
				t.Errorf("SplitTmpl() gotTbegin = %v, want %v", gotTbegin, tt.wantTbegin)
			}
			if gotTend != tt.wantTend {
				t.Errorf("SplitTmpl() gotTend = %v, want %v", gotTend, tt.wantTend)
			}
			if gotTrest != tt.wantTrest {
				t.Errorf("SplitTmpl() gotTrest = %v, want %v", gotTrest, tt.wantTrest)
			}
		})
	}
}

func Test_SplitMods(t *testing.T) {
	type args struct {
		s      string
		offset int
	}
	tests := []struct {
		name       string
		args       args
		wantTbegin int
		wantTend   int
		wantTrest  int
		wantErr    bool
	}{
		{
			name: "transfo1",
			args: args{
				"Hello World",
				0,
			},
			wantTbegin: 0,
			wantTend:   0,
			wantTrest:  11,
			wantErr:    false,
		},
		{
			name: "transfo2",
			args: args{
				"",
				0,
			},
			wantTbegin: 0,
			wantTend:   0,
			wantTrest:  0,
			wantErr:    false,
		},
		{
			name: "transfo3",
			args: args{
				"Hello |World",
				0,
			},
			wantTbegin: 6,
			wantTend:   12,
			wantTrest:  12,
			wantErr:    false,
		},
		{
			name: "transfo3",
			args: args{
				"Hello |World}",
				0,
			},
			wantTbegin: 6,
			wantTend:   12,
			wantTrest:  12,
			wantErr:    false,
		},
		{
			name: "transfo4",
			args: args{
				"Hello |Wor`}`ld}",
				0,
			},
			wantTbegin: 6,
			wantTend:   15,
			wantTrest:  15,
			wantErr:    false,
		},

		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotTbegin, gotTend, gotTrest, err := SplitM(tt.args.s, tt.args.offset)
			if (err != nil) != tt.wantErr {
				t.Errorf("SplitMods() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotTbegin != tt.wantTbegin {
				t.Errorf("SplitMods() gotTbegin = %v, want %v", gotTbegin, tt.wantTbegin)
			}
			if gotTend != tt.wantTend {
				t.Errorf("SplitMods() gotTend = %v, want %v", gotTend, tt.wantTend)
			}
			if gotTrest != tt.wantTrest {
				t.Errorf("SplitMods() gotTrest = %v, want %v", gotTrest, tt.wantTrest)
			}
		})
	}
}
