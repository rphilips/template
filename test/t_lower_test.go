package test

import (
	. "template"
	"testing"
)

func TestLower(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: "",
			r: "",
		},
		{
			t: "Hello World | lower",
			r: "hello world",
		},
		{
			t: "Hello World | lower true",
			r: "hello world",
		},
		{
			t: "Hello World | lower 1=1",
			r: "hello world",
		},
		{
			t: "Hello World | lower false",
			r: "Hello World",
		},
		{
			t: "Hello World | lower 1=0",
			r: "Hello World",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}

func TestFnLower(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: `Hello World | suffix input.lower="hello world" "OK"`,
			r: "Hello WorldOK",
		},
		{
			t: `Hello World | suffix input.lower="hello moon" "OK"`,
			r: "Hello World",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
