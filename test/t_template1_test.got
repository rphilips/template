package test

import (
	"bytes"
	"encoding/json"
	"testing"

	. "template"
)

func TestTemplate_MarshalJSON(t *testing.T) {
	type check struct {
		t string
		j string
	}
	tests := []check{
		{
			t: "",
			j: `{
				"id": "",
				"modifiers": [],
				"start": 0,
				"triples": [],
				"wspace": ""
			}`,
		},
		{
			t: "{}",
			j: `{
				"id": "{}",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": {
							"id": "",
							"modifiers": [],
							"start": 0,
							"triples": [],
							"wspace": ""
						},
						"text": ""
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "Hello $World",
			j: `{
				"id": "Hello $World",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "$World",
						"template": null,
						"text": "Hello "
					}
				],
				"wspace": ""
			}`,
		},
		{
			t: "Hello $Earth, Moon",
			j: `{
				"id": "Hello $Earth, Moon",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "$Earth",
						"template": null,
						"text": "Hello "
					},
					{
						"placeholder": "",
						"template": null,
						"text": ", Moon"
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "Hello $Earth, $Moon",
			j: `{
				"id": "Hello $Earth, $Moon",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "$Earth",
						"template": null,
						"text": "Hello "
					},
					{
						"placeholder": "$Moon",
						"template": null,
						"text": ", "
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "Hello {$Earth}",
			j: `{
				"id": "Hello {$Earth}",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": {
							"id": "",
							"modifiers": [],
							"start": 6,
							"triples": [
								{
									"placeholder": "$Earth",
									"template": null,
									"text": ""
								}
							],
							"wspace": ""
						},
						"text": "Hello "
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "Hello {$Earth}$Moon",
			j: `{
				"id": "Hello {$Earth}$Moon",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": {
							"id": "",
							"modifiers": [],
							"start": 6,
							"triples": [
								{
									"placeholder": "$Earth",
									"template": null,
									"text": ""
								}
							],
							"wspace": ""
						},
						"text": "Hello "
					},
					{
						"placeholder": "$Moon",
						"template": null,
						"text": ""
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "{$Ear1th}",
			j: `{
				"id": "{$Ear1th}",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": {
							"id": "",
							"modifiers": [],
							"start": 0,
							"triples": [
								{
									"placeholder": "$Ear1th",
									"template": null,
									"text": ""
								}
							],
							"wspace": ""
						},
						"text": ""
					}
				],
				"wspace": ""
			}`,
		},

		{
			t: "Hello | upper a b c | lower A B",
			j: `{
				"id": "Hello | upper a b c | lower A B",
				"modifiers": [
					[
						[
							{
								"nature": "i",
								"origin": "upper"
							}
						],
						[
							{
								"nature": "i",
								"origin": "a"
							}
						],
						[
							{
								"nature": "i",
								"origin": "b"
							}
						],
						[
							{
								"nature": "i",
								"origin": "c"
							}
						]
					],
					[
						[
							{
								"nature": "i",
								"origin": "lower"
							}
						],
						[
							{
								"nature": "i",
								"origin": "A"
							}
						],
						[
							{
								"nature": "i",
								"origin": "B"
							}
						]
					]
				],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": null,
						"text": "Hello"
					}
				],
				"wspace": " "
			}`,
		},

		{
			t: "Hello {| upper a b c | lower A B} World",
			j: `{
				"id": "Hello {| upper a b c | lower A B} World",
				"modifiers": [],
				"start": 0,
				"triples": [
					{
						"placeholder": "",
						"template": {
							"id": "",
							"modifiers": [
								[
									[
										{
											"nature": "i",
											"origin": "upper"
										}
									],
									[
										{
											"nature": "i",
											"origin": "a"
										}
									],
									[
										{
											"nature": "i",
											"origin": "b"
										}
									],
									[
										{
											"nature": "i",
											"origin": "c"
										}
									]
								],
								[
									[
										{
											"nature": "i",
											"origin": "lower"
										}
									],
									[
										{
											"nature": "i",
											"origin": "A"
										}
									],
									[
										{
											"nature": "i",
											"origin": "B"
										}
									]
								]
							],
							"start": 6,
							"triples": [],
							"wspace": ""
						},
						"text": "Hello "
					},
					{
						"placeholder": "",
						"template": null,
						"text": " World"
					}
				],
				"wspace": ""
			}`,
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		b, _ := json.MarshalIndent(tmpl, "", "    ")
		tdst := new(bytes.Buffer)
		json.Compact(tdst, b)

		jdst := new(bytes.Buffer)
		json.Compact(jdst, []byte(js.j))

		if jdst.String() != tdst.String() {
			t.Errorf("error in marshaling = %s\n\n%s", js.t, string(b))
		}
		if false {
			t.Errorf("\n\n\n%s\n%s", js.t, tmpl)
		}

	}

}
