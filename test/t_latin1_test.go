package test

import (
	. "template"
	"testing"
)

func TestLatin1(t *testing.T) {
	type check struct {
		t string
		r string
	}
	tests := []check{
		{
			t: "Bonjour élève $alpha",
			r: "Bonjour élève α",
		},

		{
			t: "Bonjour élève $alpha | latin1",
			r: "Bonjour élève a",
		},
		{
			t: "Bonjour élève $alpha| latin1",
			r: "Bonjour élève a",
		},
	}

	for _, js := range tests {

		tmpl, err := Compile(js.t)
		if err == nil {
			tmpl.SetID(js.t)
		}

		if err != nil {
			t.Errorf("error in compiling %s = %s", js.t, err)
		}
		r, err := tmpl.Eval(nil)
		if err != nil {
			t.Errorf("error in evaluating %s = %s", js.t, err)
		}
		if r != js.r {
			b, _ := tmpl.MarshalJSON()
			t.Errorf("\nmismatch in evaluating %s = \n       found: %s\n    expected: %s\n\n%s\n", js.t, r, js.r, string(b))
		}
	}
}
