module template

go 1.23.4

require (
	github.com/huandu/xstrings v1.4.0
	github.com/maypok86/otter v1.2.4
	github.com/mozillazg/go-unidecode v0.2.0
	github.com/spf13/cast v1.7.0
	golang.org/x/text v0.14.0
)

require (
	github.com/dolthub/maphash v0.1.0 // indirect
	github.com/gammazero/deque v0.2.1 // indirect
)
