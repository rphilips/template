package template

import (
	"strings"

	"template/pkg/expr"
)

type modifier = []expr.Postfix

type tripel struct {
	text        string
	placeholder string
	t           *Template
}

func addpexpr(modifiers []modifier, exp string) ([]modifier, error) {
	exp = strings.TrimSpace(exp)
	if exp == "" {
		return modifiers, nil
	}

	if modifiers == nil {
		modifiers = make([]modifier, 0)
	}

	ilast := len(modifiers) - 1
	if exp == "|" || ilast < 0 {
		if ilast >= 0 {
			last := modifiers[ilast]
			if len(last) == 0 {
				return modifiers, nil
			}
		}
		modifiers = append(modifiers, modifier{})
		return modifiers, nil
	}

	tokens, err := expr.InfixToPostfix(exp)
	if err != nil {
		return modifiers, err
	}
	last := modifiers[ilast]
	modifiers[ilast] = append(last, tokens)
	return modifiers, nil
}
