package template

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"github.com/maypok86/otter"

	"template/pkg/expr"
	"template/pkg/rstrings"
)

var tcache otter.Cache[string, *Template]

func init() {
	var err error
	tcache, err = otter.MustBuilder[string, *Template](100).WithTTL(time.Hour).Build()
	if err != nil {
		panic(err)
	}
}

type Template struct {
	id        string
	start     int
	tripels   []tripel
	modifiers []modifier
	wspace    string
	parent    *Template
	killed    bool
	onkill    []string
}

type Transformation struct {
	t            *Template
	placeholders map[string]any
}

type TemplateNature struct {
	data []string
	verb string
}

func (t *Template) append(text string, placeholder string, tmpl *Template) {
	if tmpl != nil {
		tmpl.parent = t
	}
	if text != "" {
		t.tripels = append(t.tripels, tripel{text, placeholder, tmpl})
		return
	}
	if placeholder == "" && tmpl == nil {
		return
	}
	ilast := len(t.tripels) - 1
	if ilast < 0 {
		t.tripels = append(t.tripels, tripel{text, placeholder, tmpl})
		return
	}
	last := t.tripels[ilast]
	if last.t != nil {
		t.tripels = append(t.tripels, tripel{text, placeholder, tmpl})
		return
	}
	if last.placeholder != "" && placeholder != "" {
		t.tripels = append(t.tripels, tripel{text, placeholder, tmpl})
		return
	}

	if placeholder != "" {
		t.tripels[ilast].placeholder = placeholder
	}
	if tmpl != nil {
		t.tripels[ilast].t = tmpl
	}
}

func (t *Template) SetID(id string) {
	t.id = id
}

func (t *Template) GetID() string {
	return t.id
}

func (t *Template) SetStart(start int) {
	t.start = start
}

func (t Template) String() string {
	var buf strings.Builder
	before := false
	for _, tripel := range t.tripels {
		before = false
		if tripel.text != "" {
			if before {
				r, _ := utf8.DecodeRuneInString(tripel.text)
				if unicode.IsLetter(r) || unicode.IsNumber(r) {
					before = true
				}
			}
			if before {
				buf.WriteRune(lr)
				buf.WriteRune(rr)
				before = false
			}
			buf.WriteString(setEscape(tripel.text))
		}
		if tripel.placeholder != "" {
			before = true
			buf.WriteString(tripel.placeholder)
		}
		if tripel.t != nil {
			buf.WriteRune(lr)
			buf.WriteString((*(tripel.t)).String())
			buf.WriteRune(rr)
		}
	}
	if t.wspace != "" {
		buf.WriteString(t.wspace)
	}

	for _, mod := range t.modifiers {
		buf.WriteRune(transfo)
		for _, expr := range mod {
			buf.WriteRune(' ')
			buf.WriteString(expr.String())
			buf.WriteRune(' ')
		}

	}

	return buf.String()
}

func (t Template) MarshalJSON() ([]byte, error) {
	j := make(map[string]any)
	j["id"] = t.id
	j["start"] = t.start

	trs := make([]map[string]any, 0, len(t.tripels))

	for _, tr := range t.tripels {
		m := make(map[string]any)
		m["text"] = tr.text
		m["placeholder"] = tr.placeholder
		if tr.t == nil {
			m["template"] = nil
		} else {
			m["template"] = *(tr.t)
		}
		trs = append(trs, m)
	}
	j["triples"] = trs

	mods := make([]any, 0, len(t.tripels))

	for _, modifier := range t.modifiers {
		smodifier := make([]any, 0, len(modifier))
		for _, expr := range modifier {
			sexpr := make([]any, 0, len(expr))
			for _, tok := range expr {
				m := make(map[string]any)
				m["origin"] = tok.Origin()
				m["nature"] = tok.Nature()
				sexpr = append(sexpr, m)
			}
			smodifier = append(smodifier, sexpr)
		}
		mods = append(mods, smodifier)
	}
	j["modifiers"] = mods
	j["wspace"] = t.wspace

	b, err := json.Marshal(j)
	return b, err
}

func Compile(s string) (t *Template, err error) {
	hex := s
	if len(hex) > 40 {
		hex = rstrings.Digest(s, rstrings.WithMaxLen(40))
	}
	t, ok := tcache.Get(hex)

	if !ok {
		t, err = compile(s, 0)
		if err != nil {
			return nil, err
		}
		tcache.Set(hex, t)
	}
	return t, nil
}

func (t *Template) Eval(placeholders map[string]any) (string, error) {

	keys := make([]map[string]any, 0)
	keys = append(keys, gplaceholders, placeholders)
	s, err := eval(t, keys)
	if err != nil {
		return "", err
	}
	if t.killed {
		return "", nil
	}

	return expr.ToString(s)
}

func eval(t *Template, keys []map[string]any) (any, error) {
	if t == nil {
		return "", nil
	}
	var buf strings.Builder
	m := make(map[string]any)
	m["wspace"] = t.wspace
	m["id"] = t.id
	keys = append(keys, m)
	for _, tripel := range t.tripels {
		if t.killed {
			return "", nil
		}
		if tripel.text != "" {
			buf.WriteString(tripel.text)
		}
		if tripel.placeholder != "" {
			val, err := value(tripel.placeholder, keys)
			if err != nil {
				return "", err
			}
			v, err := expr.ToString(val)
			if err != nil {
				return "", fmt.Errorf("placeholder `%s` has illegal type", tripel.placeholder)
			}
			buf.WriteString(v)
		}
		if tripel.t != nil {
			s, err := eval(tripel.t, keys)
			if err != nil {
				return "", err
			}
			if tripel.t.killed {
				s = ""
			}
			x, err := expr.ToString(s)
			if err != nil {
				return "", err
			}
			if x != "" {
				buf.WriteString(x)
			}
		}
	}
	if t.killed {
		return "", nil
	}

	if len(t.modifiers) == 0 {
		return buf.String(), nil
	}
	input := buf.String()
	m["oinput"] = input
	m["input"] = input
	var inp any = input
	for _, mod := range t.modifiers {
		work := mod[0]
		bok := true
		if len(mod) > 1 {
			ok, err := mod[1].Eval(keys)
			if err != nil {
				return "", err
			}
			bok, err = expr.ToBool(ok)
			if err != nil {
				return "", err
			}
			if !bok {
				continue
			}
			mod = mod[2:]
		} else {
			mod = nil
		}

		verb, err := work.Eval(keys)
		if err != nil {
			return "", err
		}
		if verb == "quit" {
			return inp, nil
		}

		inp, err = apply(verb, inp, mod, keys)
		if err != nil {
			return "", err
		}
		if t.killed {
			return "", nil
		}

		switch v := inp.(type) {
		case *Transformation:
			if len(v.placeholders) == 0 {
				inp, err = eval(v.t, keys)
			} else {
				inp, err = eval(v.t, append(keys, v.placeholders))
			}
			if err != nil {
				return "", err
			}
		case *TemplateNature:
			switch v.verb {
			case "quit":
				tnquit(t, v.data)
			case "key":
				inp = m["oinput"]
				key := v.data[0]
				value := v.data[1]
				mm := make(map[string]any)
				mm[key] = value
				keys = append(keys, mm)
			case "switch":
				key := v.data[0]
				value := v.data[1]
				found := false

				for i := len(keys); i > 0; i-- {
					val, ok := keys[i-1][key]
					if ok {
						inp = val
						found = true
						break
					}
				}
				if !found {
					return "", fmt.Errorf("key `%s` not found", key)
				}
				mm := make(map[string]any)
				mm[key] = value
				keys = append(keys, mm)
			case "kill":
				tnkill(t, v.data)
				inp = m["oinput"]
			case "id":
				tnid(t, v.data)
				inp = m["oinput"]
			}
		}
		m["input"] = inp
	}
	return inp, nil
}

func tnkill(t *Template, data []string) {
	if len(data) == 0 {
		t.killed = true
		return
	}
	ids := make(map[string]bool)
	for _, id := range data {
		ids[id] = true
	}
	tp := t
	for {
		tp = tp.parent
		if tp == nil {
			break
		}
		if ids[tp.id] {
			tp.killed = true
		} else {
			tp.onkill = append(tp.onkill, data...)
		}
	}
}

func tnid(t *Template, data []string) {
	nid := ""
	if len(data) != 0 {
		nid = data[0]
	}
	t.SetID(nid)
	for _, x := range t.onkill {
		if nid == x {
			t.killed = true
		}
	}
}

func tnquit(t *Template, data []string) {

}

func apply(verb any, inp any, mod []expr.Postfix, keys []map[string]any) (any, error) {
	sverb, err := expr.ToString(verb)
	if err == nil && sverb == "" {
		return inp, nil
	}
	args, err := evalmod(mod, keys)
	if err != nil {
		return nil, fmt.Errorf("cannot evaluate %s", mod[len(args)].String())
	}
	switch f := verb.(type) {
	case func(any, ...any) (any, error):
		return f(inp, args...)
	default:
		return nil, fmt.Errorf("modifier `%v` should start with a function", verb)
	}
}

func evalmod(mod []expr.Postfix, keys []map[string]any) (values []any, err error) {
	for _, m := range mod {
		v, err := m.Eval(keys)
		if err != nil {
			return values, err
		}
		values = append(values, v)
	}
	return values, nil
}

func value(placeholder string, placeholders []map[string]any) (val any, err error) {
	origin := placeholder
	if strings.HasPrefix(placeholder, sdollar) {
		placeholder = placeholder[1:]
	}
	for i := len(placeholders); i > 0; i-- {
		val, ok := placeholders[i-1][placeholder]
		if ok {
			return val, nil
		}
	}
	return nil, fmt.Errorf("unknown placeholder `%s`", origin)
}

func MakeTransformation(keys map[string]any, stmpl any) (*Transformation, error) {
	if stmpl == nil {
		return nil, fmt.Errorf("cannot make template out of nil")
	}
	tmpl := new(Template)
	switch v := stmpl.(type) {
	case Template:
		tmpl = &v
	case *Template:
		tmpl = v
	default:
		st, err := expr.ToString(v)
		if err != nil {
			return nil, fmt.Errorf("cannot convert to template: wrong type")
		}
		tmpl, err = Compile(st)
		if err != nil {
			return nil, fmt.Errorf("cannot convert to template: compilation error")
		}
	}
	return &Transformation{
		t:            tmpl,
		placeholders: keys,
	}, nil
}

func MakeTemplateNature(verb string, data []string) (*TemplateNature, error) {
	return &TemplateNature{
		verb: verb,
		data: data,
	}, nil
}
