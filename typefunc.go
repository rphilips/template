package template

import (
	"fmt"
	"reflect"
	"time"

	"template/pkg/dati"
	"template/pkg/expr"
)

func ToString(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("string works without additional arguments")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	return str, nil
}

func ToInt(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("int works without additional arguments")
	}
	i, err := expr.ToInt64(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to int64")
	}
	return i, nil
}

func ToBool(s any, args ...any) (any, error) {
	if len(args) > 0 {
		return nil, fmt.Errorf("bool works without additional arguments")
	}
	b, err := expr.ToBool(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to bool")
	}
	return b, nil
}

func Time(s any, args ...any) (any, error) {
	if len(args) == 0 {
		args = append(args, time.Now())
	}
	if len(args) > 2 {
		return nil, fmt.Errorf("time works with 1 argument")
	}
	lg := ""
	var err error
	if len(args) == 2 {
		lg, err = expr.ToString(args[1])
		if err != nil {
			return nil, fmt.Errorf("cannot convert language argument to string")
		}
	}
	tmpli, timi, err := tmplTim(s)

	if err != nil {
		return nil, fmt.Errorf("cannot find a template or a time argument to string")
	}

	tmpl1, tim1, err := tmplTim(args[0])

	if err != nil {
		return nil, fmt.Errorf("cannot find a template or a time argument to string")
	}

	if tmpl1 == nil && tmpli == nil {
		return nil, fmt.Errorf("cannot find a template")
	}

	if timi == nil && tim1 == nil {
		return nil, fmt.Errorf("cannot find a time argument")
	}

	tim := timi
	if tim == nil {
		tim = tim1
	}

	k := make(map[string]any)
	for x, v := range dati.Parts(tim, lg) {
		k[x] = v
	}
	tmpl := tmpli
	if tmpl == nil {
		tmpl = tmpl1
	}
	return MakeTransformation(k, tmpl)
}

func tmplTim(s any) (any, *time.Time, error) {
	switch ss := s.(type) {
	case time.Time:
		if reflect.DeepEqual(ss, time.Time{}) {
			ss = time.Now()
		}
		return nil, &ss, nil
	case *time.Time:
		if ss == nil {
			x := time.Now()
			ss = &x
		}
		return nil, ss, nil
	case *Template:
		return s, nil, nil
	case Template:
		return &s, nil, nil
	default:
		x, err := expr.ToString(s)
		return x, nil, err
	}
}
