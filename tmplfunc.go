package template

import (
	"fmt"
	"template/pkg/expr"
	"unicode"
	"unicode/utf8"
)

func Kill(s any, args ...any) (any, error) {
	sargs := make([]string, 0, len(args))
	for _, arg := range args {
		x, err := expr.ToString(arg)
		if err != nil {
			return nil, fmt.Errorf("cannot convert arg to string")
		}
		sargs = append(sargs, x)
	}
	return MakeTemplateNature("kill", sargs)
}

func ID(s any, args ...any) (any, error) {
	sargs := make([]string, 0, len(args))
	for _, arg := range args {
		x, err := expr.ToString(arg)
		if err != nil {
			return nil, fmt.Errorf("cannot convert arg to string")
		}
		sargs = append(sargs, x)
	}
	return MakeTemplateNature("id", sargs)
}

func Key(s any, args ...any) (any, error) {
	min := 1
	max := 1
	l := len(args)
	if l < min && l > max {
		return nil, fmt.Errorf("number of arguments for key is 1")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	key, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert key name to string")
	}
	if key == "" {
		return nil, fmt.Errorf("key name is empty")
	}
	w, _ := utf8.DecodeRuneInString(key)

	if !unicode.IsLetter(w) {
		return nil, fmt.Errorf("`%s` should start with a letter", key)
	}
	for _, r := range key {
		if !unicode.IsLetter(r) && !unicode.IsNumber(r) {
			return nil, fmt.Errorf("`%s` should only consist of letters or numbers", key)
		}
	}

	data := []string{key, str}
	return MakeTemplateNature("key", data)
}

func Switch(s any, args ...any) (any, error) {
	min := 1
	max := 1
	l := len(args)
	if l < min && l > max {
		return nil, fmt.Errorf("number of arguments for key is 1")
	}
	str, err := expr.ToString(s)
	if err != nil {
		return nil, fmt.Errorf("cannot convert input to string")
	}
	key, err := expr.ToString(args[0])
	if err != nil {
		return nil, fmt.Errorf("cannot convert key name to string")
	}
	if key == "" {
		return nil, fmt.Errorf("key name is empty")
	}
	w, _ := utf8.DecodeRuneInString(key)

	if !unicode.IsLetter(w) {
		return nil, fmt.Errorf("`%s` should start with a letter", key)
	}
	for _, r := range key {
		if !unicode.IsLetter(r) && !unicode.IsNumber(r) {
			return nil, fmt.Errorf("`%s` should only consist of letters or numbers", key)
		}
	}

	data := []string{key, str}
	return MakeTemplateNature("switch", data)
}
